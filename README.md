# danielpeukert.cz

## Development
* `npm run dev` - watches the `src` directory and rebuilds when needed

## Deployment
### Manual
* `npm run prod` - builds and minifies the `src` directory
* upload the contents of the `dist` directory to a webhost of your choice

### Docker image
* `registry.gitlab.com/dpeukert/danielpeukert.cz` ([tag list](https://gitlab.com/dpeukert/danielpeukert.cz/container_registry))

## License
The contents of this repository are provided under the GPLv3 license or any later version (SPDX identifier: `GPL-3.0-or-later`) with the following exceptions:
* The [Overpass](https://fonts.google.com/specimen/Overpass) font from the [Google Fonts](https://fonts.google.com) font library by [Delve Withrington](https://www.delvefonts.com) licensed under the [SIL Open Font License 1.1](https://openfontlicense.org/open-font-license-official-text/)
  * `src/cv/fonts/Overpass_*.*`
* The [Open Sans](https://fonts.google.com/specimen/Open+Sans) font from the [Google Fonts](https://fonts.google.com) font library by [Steve Matteson](https://mattesontypographics.com) licensed under the [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0.txt)
  * `src/cv/fonts/Open_Sans.*`
* The [Leaflet](https://leafletjs.com) map library by [Volodymyr Agafonkin](https://agafonkin.com) licensed under the [BSD 2-Clause License](https://github.com/Leaflet/Leaflet/blob/v1.9.4/LICENSE)
  * `src/fpv/mapa/js/leaflet.js`
  * `src/fpv/mapa/css/leaflet.css`
  * `src/fpv/mapa/img/marker-*.png`
* The introduction text and data regarding FPV spots from [live.rotorama.cz/fpvspots](https://live.rotorama.cz/fpvspots/) with no explicit license, used with an attribution on the relevant section of the site
  * `src/fpv/mapa/index.html`
  * `src/fpv/mapa/js/main.js`
