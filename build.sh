#!/bin/sh

# Delete the old dist folder
rm -rf dist/

# Make a copy of our source
cp -r src/ dist/

# Compile the SCSS to regular CSS
npx sass dist/

# Remove the SCSS files
find './dist/' -type f -name '*.scss' -delete
