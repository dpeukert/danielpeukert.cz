#!/bin/sh

realip_path='/etc/nginx/conf.d/realip.conf'

if [ -n "$REAL_IP_HEADER" ] && [ -n "$REAL_IP_FROM" ]; then
	echo -e "real_ip_header $REAL_IP_HEADER;\nset_real_ip_from $REAL_IP_FROM;" > "$realip_path"
elif [ -f "$realip_path" ]; then
	rm -f "$realip_path"
fi

exec nginx -g 'daemon off;'
