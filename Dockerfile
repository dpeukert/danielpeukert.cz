FROM node:23.8.0-alpine AS build
WORKDIR /usr/src/app
COPY ./ ./
RUN npm install
RUN npm run prod

FROM nginx:1.27.4-alpine AS serve
WORKDIR '/usr/share/nginx/html'
RUN rm -rf *
COPY --from=build '/usr/src/app/dist/' './'
COPY 'headers.conf' '/etc/nginx/conf.d/headers.conf'
COPY 'start.sh' '/usr/local/bin/danielpeukertcz-start'
RUN chmod 755 '/usr/local/bin/danielpeukertcz-start'
CMD ["danielpeukertcz-start"]
EXPOSE 80
HEALTHCHECK --interval=60s --timeout=10s CMD wget --tries=1 --spider http://127.0.0.1:80/ || exit 1
