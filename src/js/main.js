// @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-3.0
'use strict';

const lightRadio = document.querySelector('.color-mode-switcher#light');
const darkRadio = document.querySelector('.color-mode-switcher#dark');
const cvLink = document.querySelector('#cv-link');
const body = document.querySelector('body');

// Check if we have all the elements we need
if (lightRadio === null || darkRadio === null || cvLink === null || body === null) {
	throw new Error('Not all required elements are present');
}

// Check if the user's language is Czech, if so, replace the default English CV link
if (navigator.language.toLowerCase().includes('cs') === true) {
	cvLink.setAttribute('href','cv/cs');
}

// Disable transitions temporarily
body.classList.remove('transitions');

// If we have a mode in local storage, use it to check its respective radio button
switch (localStorage.getItem('mode')) {
	case 'light':
		lightRadio.checked = true;
		break;
	case 'dark':
		darkRadio.checked = true;
		break;
}

// Trigger a CSS reflow
body.offsetHeight;

// Reenable transitions
body.classList.add('transitions');

// When a radio button is checked, save the active mode to local storage
for (const radio of [lightRadio, darkRadio]) {
	radio.addEventListener('change', () => {
		if (radio.checked) localStorage.setItem('mode', radio.id);
	});
}
// @license-end
