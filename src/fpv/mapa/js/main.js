const blue = L.icon({
	iconUrl: 'img/marker-blue.png',
	shadowUrl: 'img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

const gold = L.icon({
	iconUrl: 'img/marker-gold.png',
	shadowUrl: 'img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

const green = L.icon({
	iconUrl: 'img/marker-green.png',
	shadowUrl: 'img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

const red = L.icon({
	iconUrl: 'img/marker-red.png',
	shadowUrl: 'img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

const markers = [{
	'title': 'Alej stromu za Penny',
	'description': 'Rada stromu idealni na trenink slalomu. Místo není moc velké - cca 10x150m ale je z jedne strany plot a z druhe volné prostrastvi, trava je sekana a jen obcas se objevi vencici lide jinak se tu nikdo nevyskytuje',
	'x': 50.125854,
	'y': 14.1222918,
}, {
	'title': 'Anthropos',
	'description': 'Opuštěné místo, obrněný transporter, spousta překážek, vhodné i na parkour.',
	'youtube': [
		'j94JTtfr8BU',
	],
	'x': 49.19002896403,
	'y': 16.565071605067,
}, {
	'title': 'Areál bývalej Frucony',
	'x': 49.108937227814,
	'y': 21.081807900051,
}, {
	'title': 'Areál Interlovu',
	'description': 'Ideální přístup je od OBI Roztyly, tam zaparkovat a jít cestou přes les. Dá se létat i na discgolf hřišti (pokud tam nikdo není) nebou cestou v lesoparku bývá taky klid. Samotný areál je prázdný a zatím nehlídaný, ale časem se tam asi bude stavět.',
	'x': 50.038368488686,
	'y': 14.471029878132,
}, {
	'title': 'Areál Zámostí Albrechtice',
	'description': 'Skvělé místo pro lítání v areálu restaurace',
	'x': 49.78053664537,
	'y': 18.529322147369,
}, {
	'title': 'Areál zdraví',
	'description': 'super místo, pozor na pěšínku uprostřed. v létě si tu občas lidé zkracují cestu na koupák',
	'x': 50.0972593,
	'y': 14.3135655,
}, {
	'title': 'Auto/motokrossová trať',
	'description': 'Krásná trať, vhodná spíš pro éro, nebo větší quad. Bod na mapě je místo, ze kterého se dá obletět celá trať. Moc často se tam nejezdí. Jenom bacha na dráty mezi tlampačema.',
	'x': 49.4851511,
	'y': 17.4313331,
}, {
	'title': 'Autocamp Výr',
	'description': 'Klidne místo, na chill let ideální',
	'x': 48.928503824293,
	'y': 16.109255338728,
}, {
	'title': 'AVALON',
	'description': 'Velmi riedky les, ideál na freestyle. Prístup s autom až na spot po polnej ceste.',
	'youtube': [
		'3X25Gc0rM4k',
	],
	'x': 48.2607414,
	'y': 17.9696953,
}, {
	'title': 'Bando LDN Bílovice',
	'description': 'Totálně boží bando, jeden z nej spotů poblíž Brna, level: hard, k dispozici 5 pater, schody, sklep a okolí budovy s výtahovou šachtou, doporučuju!',
	'youtube': [
		'joH1yumd_dY',
	],
	'x': 49.27575036396,
	'y': 16.669095822785,
}, {
	'title': 'Barrandov',
	'description': 'Zarostlá ale celkem fotogenická malá louka vedle Barrandovských studií. Parkování hned vedle u Lidlu. Dá se jít i dál podívat se na kulisy středověkého města v rámci Barrandovských studií, ale tam se přímo lítat nedá, je to obehnané plotem a hlídané. Ještě dál směrem na Slivenec jsou pak louky/ pole a bezdomovecká kolonie :)',
	'x': 50.031107272689,
	'y': 14.382378399623,
}, {
	'title': 'Betonárka',
	'description': 'Skvělý spot, luxusní proplétání malou industriální zónou. V provozu, ideální navštívit přes víkend nebo velmi brzo ráno, či pozdě večer. Pokud někoho potkáte, buďte zdvořilí, ať si na spotu zalítáme všichni :)',
	'youtube': [
		'2o5tJI0heJg',
	],
	'x': 49.267984183945,
	'y': 17.731385075419,
}, {
	'title': 'Betonárka Chodov',
	'description': 'Velký prostor na létání ohraničený vysokým valem, bez lidí, je tu i betonový plac na přistávání letadel. Je zde i trvalá RC dráha pro auta. Parkování u brány z uice Na Jelenách nebo Cigánkova, nedá se zajet až do areálu.',
	'images': [
		'https://lh6.googleusercontent.com/bCgYQzTax9cP8T5WHh6o9VZ3CSVouLLl0A4Jcpr1ZIocCYJVA5Qkvl3wXN5TuJjZNm9JcvzYQDFiNwdfNoNaOIxKnWGtxMVvCKyMN1f6RoIouKeF08RPmtqzFosMORI',
	],
	'x': 50.020983,
	'y': 14.505676,
}, {
	'title': 'Bohdašín hřiště',
	'description': 'Les, louka, branky...',
	'images': [
		'https://lh4.googleusercontent.com/GO8KP3AnbxX1vUWug0AJTyd0AJAOSvrvQ_-mdL1GG8aebLGFrtY4g28HbskCuTz8LIPJxWA43MDKtZIqbTASxNnCXEYxkao-mk7gnQAJ14WOpIuO_X7gn1GknWZrc4yt'
	],
	'x': 50.5027073,
	'y': 16.1045194,
}, {
	'title': 'Bohutínské opuštěné fotbalové hřiště',
	'description': 'Opuštěné fotbalové hřiště 50 metru od silnice na Rožmitál. Třikrát do roka využité místní ZŠ, jinak krásná samota. Obsahuje několik prastarých kovových branek na malou kopanou, klasické dvě velké fotbalové branky a několik starých překážek pro psy.. Obehnáno dokola cestou, lemovanou topoly. Na místě jsem začínal s FPV a rád se tam vrátím. DVR video z místa (pilotáž ignorovat, lítal jsem třetí týden).',
	'youtube': [
		'kC21wVMYDR0',
	],
	'x': 49.6534046,
	'y': 13.9456415,
}, {
	'title': 'Borová Malenovice',
	'description': 'Longrange, velký prostor, výhled, altán kde se dá sednout, proletět',
	'x': 49.586679268999,
	'y': 18.396750958983,
}, {
	'title': 'Branická louka',
	'description': 'Zaparkovat se dá hned vedle v ulici U kempinku. Celkem úzká louka.',
	'x': 50.028889887376,
	'y': 14.400793920721,
}, {
	'title': 'Březiny',
	'x': 50.763159608493,
	'y': 14.251865337519,
}, {
	'title': 'Brezovy haj',
	'youtube': [
		'Rgsggiwt6Vo',
	],
	'x': 49.114817,
	'y': 18.9973462,
}, {
	'title': 'Brownfiled spot hned u Mendláku',
	'description': 'Obří opuštěný pozemek, parovod na skvělý powerloops. Level: medium',
	'youtube': [
		'N_0R68-JkXo',
	],
	'x': 49.188470181022,
	'y': 16.592507850279,
}, {
	'title': 'BuBkA spots :)',
	'description': 'Pěkná plocha pro létání. Mezi Rájcem a Jestřebím. Vhodné například pro Longrange lety.',
	'x': 49.847091747971,
	'y': 16.894062233199,
}, {
	'title': 'BuBkA spots :)',
	'description': 'Zřícenina hradu Brníčko. Když vyrazíte na procházku, můžete ji zakončit FPV kochačkou :) Dá se pěkně prolétávat okolo hradu. Hrad je ale celkem frekventované místo pro turisty. TAK OPATRNĚ ! :)',
	'x': 49.895267203413,
	'y': 16.972367281908,
}, {
	'title': 'BuBkA spots :)',
	'description': 'Zajímavé místo - LOM. Oficiálně je vstup do Vápencového lomu zakázán. Dá se ale legálně zaparkovat na místě, které je zaznačeno v mapě.',
	'x': 49.701546494534,
	'y': 17.002041712299,
}, {
	'title': 'Burešovy sady',
	'description': 'Alej stromů a louka na dostatečné vyblbnutí. Spot slouží také jako vyhlídka na město Zlín. Nejkrásnější je to zde při západu slunce.',
	'x': 49.2323641,
	'y': 17.6950854,
}, {
	'title': 'Bývalá letištní plocha',
	'description': 'Volné prostranství',
	'x': 49.026481253111,
	'y': 17.465772544656,
}, {
	'title': 'Bývalá motokrosová trať',
	'description': 'Nedaleko je bývalá motokrosová trať " hvězda" , v místě stará rozhledna ( dá se proletět ), ráno a podvečer pěkný výhled za slunného dne. V okolí méně hustý porost nízkých stromků na trénink race. O víkendu v ranních hodinách sem občas zavítají kluci na endurech nebo krosech, takže je možno něco natočit',
	'x': 49.258116566354,
	'y': 17.406509363995,
}, {
	'title': 'Bývalé vojenské cvičiště Bzenec',
	'description': 'Hodně osamocených stromů, členitý terén, písečná půda',
	'x': 48.9555373,
	'y': 17.2908524,
}, {
	'title': 'Čerňák',
	'description': '"Pyramida" s plochou nahoře, občas tam jsou paraglidisti nebo kitisti, když víc fouká. Přístup od KFC kolem MAKRO zleva a tunelem pod kolejemi (je tam vyšlapaná cesta) a potom chvíli rovně do kopce lesíkem a pak doprava (hned doprava po tunelu nedoporučuji - bezďáci tam mají stanové městečko).',
	'x': 50.11469504351,
	'y': 14.582216460253,
}, {
	'title': 'Černovické terasy',
	'description': 'Provoz malý, pejskaři v tomto místě parkují a chodí po asfaltové cestě za závorou. Na louku by neměl být problém dát branky. Je to kousek od letiště, ale s miniquadama není problém. Občas zde bývají s kitem<br>Bývá silnější vítr',
	'x': 49.1641817,
	'y': 16.6699934,
}, {
	'title': 'Černý Most',
	'description': 'Louka se stromky, které nabízejí možnost sestavit si různorodou trať.',
	'x': 50.0995649,
	'y': 14.5761538,
}, {
	'title': 'Čerťák',
	'description': 'Spousta stromů, louka v létě pivo. Nutné dávat pozor na výletníky',
	'x': 49.56796394265,
	'y': 18.000379800797,
}, {
	'title': 'Cerveny kamen park',
	'description': 'Velky udrziavany park s vela prirodnymi prekazkami. Cez vikend je tam hustejsia premavka ale je to mimo hlavny tah na hrad. Super miesto.',
	'youtube': [
		'jTECgnsegYQ'
	],
	'x': 48.3955875,
	'y': 17.3377562,
}, {
	'title': 'ČKD Kolbenova',
	'description': 'Opuštěná budova pro indoor létání... velké dlouhé haly se sloupama.',
	'images': [
		{
			'link': 'http://www.fotolokace.cz/detail/ckd-kolbenova-70',
		},
	],
	'x': 50.1114315,
	'y': 14.5095009,
}, {
	'title': 'Cukrák',
	'description': 'Na místě pokáceno mnoho stromů, pravděpodobně kvůli kůrovci. Spousta místa kde létat, sem tam osamocená borovice. Nutno létat minimálně 100m od vysílače. Po stezce občas někdo projde. Nelétejte blízko dálnice',
	'x': 49.940527747909,
	'y': 14.362079671591,
}, {
	'title': 'Cyklostezka Chválkovice - Černovír',
	'description': 'Kousek od cyklostezky z Chválkovic do Černovíra a Hlušovic. Cesta je dostatečně daleko, aby se cyklisti necítili ohrožení. Prostorná louka na branky plus nějaké stromy jako přirozené překážky. Jen jednou jsem tu narazil na zvědavého pejskaře, ale jinak je tu klid.',
	'x': 49.620088496053,
	'y': 17.28158403423,
}, {
	'title': 'Cyklostezka u Otvic',
	'description': 'Cyklostezka z Otvic na Strupčice',
	'x': 50.481739406794,
	'y': 13.463298751348,
}, {
	'title': 'D8 dálniční most',
	'description': '300m dlouhý koridor pod dálničním mostem - ideální místo v deštivých dnech. Doporučuji příjezd po zpevněné polní cestě od firmy Eso-land.',
	'x': 50.440358,
	'y': 14.1671893,
}, {
	'title': 'Dirty Pardubice',
	'description': 'když nejezděj bajkeři, dá se lítat trať dokola, případně na louce.',
	'youtube': [
		'YGBj2s8Y9tM',
	],
	'x': 50.0415483,
	'y': 15.7627898,
}, {
	'title': 'Dívčí hrady',
	'description': 'Členitost: Louka se stromky, super na tratě, ohraničeno školkou malých stromků<br>Parkování autem: Ne, nejbližší místo (50.05208, 14.39496) ale i to je za zákazem.<br>Frekventovanost: V létě asi budou lidi chodit po té cestě uprostřed na stezku, takže na to pozor',
	'images': [
		'https://lh5.googleusercontent.com/4vZKujkIB9bsMpOztF2b6semtETA2_LRGz71XDLok3u-2htIbiN0KI0Ni0yxtySNEoWXZUR8HDWxh_QTxwZY54Fy3kUITd2OnS6PyYmV4Fm7xK4-n1r54SY29Z17ASGs-w',
	],
	'x': 50.046889,
	'y': 14.3959375,
}, {
	'title': 'Dívčí hrady/ Ctirad',
	'description': 'Několik velikých luk, lidé tam pouští draky i tam létají RC modeláři. Na freestyle je tam pár zajímavých stromů.',
	'x': 50.049427041289,
	'y': 14.403588414661,
}, {
	'title': 'Dlouhá Louka',
	'x': 49.0765998,
	'y': 13.9783752,
}, {
	'title': 'Drone plac FSH',
	'description': 'Je to luka v technologickom parku, je tam kopec naletovych drevin celkom fajn sa tam lieta ale idealne az po 18 tej hodine dovtedy je na prilahlom parkovisku dost aut.',
	'x': 48.8872948,
	'y': 18.0004549,
}, {
	'title': 'Drone plac Soblahov',
	'description': 'Len luka da sa tam prist autom zopar stromov tam je ako orientacia.',
	'x': 48.8628797,
	'y': 18.0997825,
}, {
	'title': 'Družstvo Terezov',
	'youtube': [
		'N7g6UuWKPCg',
	],
	'x': 48.4020768,
	'y': 17.7517713,
}, {
	'title': 'Dubicko u Lesa',
	'x': 49.825109412405,
	'y': 16.976815812456,
}, {
	'title': 'Dubnica',
	'description': 'Fajn miesto na lietanie. Bezproblemovy pristup autom.',
	'x': 48.9548328,
	'y': 18.1880593,
}, {
	'title': 'Farma ošípaných - už len ruiny',
	'description': 'Parkovanie priamo na farme, opustené miesto sem tam prejde nejaké auto alebo traktor. Overené miesto ani policajti so mnou nemali žiadny problém iba prešli okolo mňa obehli farmu autom a dovidenia ja som lietal ďalej.',
	'x': 47.9881222,
	'y': 18.2955759,
}, {
	'title': 'Fčelin...',
	'description': 'Spoustu krásných stromů. Terénní elevace. Stromy rostou daleko od sebe. Pod stromy je to vyčištěné.',
	'x': 49.399616192456,
	'y': 16.022486505819,
}, {
	'title': 'Fialka Unhošť',
	'x': 50.094325530299,
	'y': 14.134874127261,
}, {
	'title': 'Fotbalové hřistě Drahelice',
	'description': 'Tady lítáme často, je zde chládek a lavičky, ještě nás odsud nikdo nevyhnal. občas zde hrajou fotbal.',
	'x': 50.1781211,
	'y': 15.0232565,
}, {
	'title': 'Fotbalové hřiště u stadionu za Lužánkama',
	'description': 'Skvělý spot na testing a zalítání. Level: beginner',
	'youtube': [
		'oV9_-m9bWlA',
	],
	'x': 49.213351180688,
	'y': 16.613243989957,
}, {
	'title': 'FPV Aréna Plzeň',
	'description': 'FPV aréna Správy informačních technologií města Plzeň. Aréna slouží k testování dronů, tréninkům i organizaci závodů. Létání možné po domluvě s Tomášem Cholinským - cholinsky@plzen.eu.',
	'images': [
		'https://live.rotorama.cz/wp-content/uploads/2021/02/148867176_693055788054449_6814272612975552077_n-300x87.png',
	],
	'youtube': [
		'cjw7oiBn7gg',
	],
	'arena': true,
	'x': 49.736404496647,
	'y': 13.381907224366,
}, {
	'title': 'FPV Crew Mělník',
	'description': 'Organizované létání na stále trati, která je chráněna sítí. Místo je často využíváno pro trénink i ligové a přátelské závody. Létání je možné po domluvě se správci. Více informací na <a href="https://www.facebook.com/groups/165414084157610" target="_blank">FB skupině</a>.',
	'images': [
		'https://live.rotorama.cz/wp-content/uploads/2021/02/FPVCrewMelnik-300x58.png',
	],
	'youtube': [
		'c6ozQqbf8z8',
	],
	'arena': true,
	'x': 50.394983025356,
	'y': 14.472900306693,
}, {
	'title': 'Frantiskov',
	'x': 50.7682494,
	'y': 15.0370002,
}, {
	'title': 'Freestyle Heaven',
	'description': 'Perfektné miesto na freestyle, prípadne pri použití gates ideálny racing spot.',
	'images': [
		{
			'image': 'https://preview.ibb.co/hbgMw9/20181012-164101.jpg',
			'link': 'https://ibb.co/fBpkOp',
		},
		{
			'image': 'https://preview.ibb.co/kedC3p/20181012-164153.jpg',
			'link': 'https://ibb.co/c3OO9U',
		},
		{
			'image': 'https://preview.ibb.co/cbO1w9/20181012-164314.jpg',
			'link': 'https://ibb.co/jCdqpU',
		},
	],
	'x': 49.139865042924,
	'y': 19.392619314753,
}, {
	'title': 'Globus Havířov',
	'description': 'Perfektní místo jak pro race, tak i freestyl. Velká louka se stromy. Parkování přímo na místě.',
	'x': 49.793014220671,
	'y': 18.440459718639,
}, {
	'title': 'Hamerky',
	'description': 'Bývalá motokrosová dráha na Vysočině.',
	'youtube': [
		'lto4C_r4ISk',
	],
	'x': 49.307973,
	'y': 15.084303,
}, {
	'title': 'Hamerky',
	'youtube': [
		'pL2Afj1YRAE',
	],
	'x': 49.312996,
	'y': 15.084154,
}, {
	'title': 'Harém u Edy',
	'x': 49.673297850963,
	'y': 17.194186250671,
}, {
	'title': 'Havířov',
	'description': 'Zde lítá skupina QUADLAND-OV. Když budete mít někdo cestu k nám, rádi vás uvidíme',
	'x': 49.8025966,
	'y': 18.4447575,
}, {
	'title': 'Havířov Louka',
	'description': 'Obrovská louka s velkým množstvím stromů, tráva je pravidelně sečená',
	'x': 49.778109530887,
	'y': 18.420613874251,
}, {
	'title': 'Hilton (stará veľkovýkrmňa HD)',
	'description': 'Prístup je od križovatky pri pošte v Hronovciach, okolo družstva sa dá prejsť ale je to len poľná cesta miestami necesta.. Na spote pozor hlavne dole vo výťahovej šachte, vyzerá, že je to len plné bordelu ale je to len bordel na vode , tak bacha na to.. spot je inak RF hell, takže bacha na to',
	'x': 47.9924491,
	'y': 18.5998535,
}, {
	'title': 'Hlohovec Svah',
	'description': 'Mierny svah s výhľadom na celý Hlohovec. Na časti kopca je vysoká tráva vhodná na pristátie bez podvozku, zbytok je obrovská rola',
	'x': 48.4430097,
	'y': 17.8234291,
}, {
	'title': 'Holešov, bývalé letiště',
	'description': 'Veliký volný prostor, myslím si, že je to vhodné místo na závodní akce.',
	'x': 49.319963146659,
	'y': 17.5702364569,
}, {
	'title': 'Homolka nad Chuchlí',
	'description': 'Krásné místo s úzkým výhledem na Modřany přes Vltavu a starou Chuchli. Kopeček je zajímavý tím, že když stojíte na nevelkém vrcholu, terén od Vás na všechny strany jenom klesá. Homolka je přírodní rezervace.<br>Spíše na videodrony, severně je Chuchelský háj a zarostlé louky. Terén dost členitý, a zarostlý.<br>Dá se zaparkovat tak, že to máte od auta zrhuba 400 m do kopce.',
	'youtube': [
		'jXhZ1T9Qb2Q',
	],
	'x': 50.0152034,
	'y': 14.373301,
}, {
	'title': 'Horní Suchá',
	'description': 'Slušné místo pro freestyle, je tam pár trubek pod kterýma se dá podlétávat, dvě louky a dlouhá rovinka',
	'x': 49.813062059654,
	'y': 18.468052680978,
}, {
	'title': 'Hospoda',
	'description': '3 prostorne louky oddelene remizky se stromy kolem, ktery se da pekne litat. Moznost dojet autem az na misto.',
	'x': 50.138216434349,
	'y': 13.890598631404,
}, {
	'title': 'Hostivická sušička',
	'description': 'Pole, stromy, scházíme se zde i s brankami a vlajkami.',
	'images': [
		'https://lh6.googleusercontent.com/PDPD5isPkvJi90yXwWvjuZFrnVS51aGhcdNz-5luv8LEEDFUjMapbcoRgV7ow9d-GNRgLWydBySKdQeJpR_9MWB8YcDeixrUkhN6alwhOShu9W_VonhYzjnLIYmAjqkQ4g',
	],
	'x': 50.0743286,
	'y': 14.2210722,
}, {
	'title': 'Hrabůvka Halda',
	'description': 'Na místo chodí poměrně málo lidí, zřejmě, protože jsou cesty těžko dostupné, jelikož jsou zarostlé trávou. Je to rozsáhlé místo s jezírkem. Prostor nabízí hodné míst pro freestyle, navíc je kousek za jezerem uzavřená menší hala. Občas se tam vyskytují opilí mladiství a různá individua, avšak nijak nikdy neotravovali, ale vůči věcem bych byl obezřetný. Nejklidněji se tam létá dopoledne. Prostor kolem označeného bodu na mapě, nabízí nejklidnější místo, kde chodí velmi málo lidí.',
	'x': 49.787477423856,
	'y': 18.275794676512,
}, {
	'title': 'Hrádza Madunice',
	'youtube': [
		'OJdY9TG0BSI',
	],
	'x': 48.551081,
	'y': 17.7826731,
}, {
	'title': 'Hřibsko',
	'description': 'Proste pole, na trenink dobry :-)',
	'youtube': [
		'Rofbv4BQQyA',
	],
	'x': 50.2014143,
	'y': 15.7308206,
}, {
	'title': 'Hřiště Černovír Stratilova',
	'description': 'Místo vhodné spíš pro blbnutí s micro-quadem. Je to blízko domů, vody a hlavně dětského hřiště. Ráno tu bývá klid - sem tam pejskař. Ale je potřeba počítat s tím, že je to frekventované místo.',
	'x': 49.610168059599,
	'y': 17.256838247344,
}, {
	'title': 'Hřiště SOKOL Havířov',
	'description': 'Hřiště střední velikosti. Sice v zastavěné oblasti, ale zatím bez problémů s domorodci.',
	'x': 49.79545295457,
	'y': 18.416696190834,
}, {
	'title': 'Hruska01',
	'description': 'Velka louka na kopci. Bud se tu da postavit zavodni trat nebo vystartovat na vylet nad blizke udoli a les. Pozor nachazi se v okruhu maleho letiste a je treba se nejdriv domluvit.',
	'x': 49.104084637864,
	'y': 17.748129277029,
}, {
	'title': 'Hruska02',
	'description': 'Velka louka vhodna na postaveni zavodni trate pripadne lehky freestyle na prilehle pasece.',
	'x': 49.109232997737,
	'y': 17.755221111003,
}, {
	'title': 'Husova',
	'x': 50.749687348945,
	'y': 14.30731276439,
}, {
	'title': 'Jabloňka',
	'description': 'Trávnatá plocha 130x60 metrov a skala vysoká 80 metrov vhodná na dive. Vhodné pro menší drony, 5" ohrožuje při každém triku kolemjdoucí. O víkendech se občas používá plácek jako parkoviště.',
	'images': [
		'https://lh3.googleusercontent.com/BnH27GEnT6NRUPWhppU9-3UlxTiIUN9pPNDDwPgbwuRYgw7M9YWtgs7FXmtdYNdo_tMHnkA-nAi_OlX3evPj2kofxgTL9Yhb3A5rSff7LyUpSpsKccIRA9haLW1W6PI',
	],
	'youtube': [
		'jz0FVmyPRfI',
	],
	'x': 50.1155458,
	'y': 14.4393987,
}, {
	'title': 'Jazdecká dráha Topoľčianky',
	'description': 'Pri vstupe zo zadu mozu zaparkovat 3 auta. Na drahe su prekazky z malych krickov. Koseny stravnik',
	'x': 48.4286097,
	'y': 18.4234071,
}, {
	'title': 'JRD Černík',
	'description': 'Bývalé družstvo. Teraz už len ruiny. So vstupom do areálu nieje problém. Parkovanie je možné len pred bránou nakoľko je na príjazdových areálových bet. plochách veľké množstvo skla. A hlavne pozor na možné diery v zemi, jedná sa obývalé žumpy s chýbajúcimi poklopmi.',
	'youtube': [
		'h2W-mC8uS3c',
	],
	'x': 48.1573259,
	'y': 18.2322407,
}, {
	'title': 'Kaple Svaté Anny',
	'description': 'Jsou tam nějaké lavičky a nějaké opravdu vzrostlé stromy. Kolem je pole a na místě se nikdo moc nepotuluje. Je tam malá kaplička, spíš jen taková železná konstrukce vhodná na průlety. Okolní cesty jsou pro zemědělskou techniku.',
	'x': 49.934631736061,
	'y': 17.859241962433,
}, {
	'title': 'Kaplička',
	'description': 'Pole, školka stromů. Široko daleko nikdo. Místní (i PČR) jsou na koptéry zvyklí. Letadla cca 600m, přímo pod korydorem.',
	'x': 50.143786369845,
	'y': 14.369969059564,
}, {
	'title': 'Karviná',
	'description': 'Skvělý spot na FPV ale trochu delší tráva, takže najít koptéru je trošku challenge',
	'x': 49.853143873182,
	'y': 18.510167105282,
}, {
	'title': 'Karviná',
	'description': 'Velký travnatý prostor pro race , hodně stromků.',
	'x': 49.843483202937,
	'y': 18.507488965988,
}, {
	'title': 'Kasárna Bílý vrch',
	'description': 'Opuštěné místo, spousta budov, ideál na freestyle. Nehlídané.',
	'x': 50.1274635,
	'y': 14.6335638,
}, {
	'title': 'Kaufland NJ',
	'description': 'Park se stromy. Nutné dávat pozor na pejskare.',
	'x': 49.587126375493,
	'y': 17.999795079231,
}, {
	'title': 'Kavčí hory',
	'description': 'Cca 70m x 70m. Bývalé baseballové hřiště, krátký udržovaný trávník, pár stromů. Pejskaři chodí po cestách, parkovat se dá hned vedle. Vhodné pro LOS i FPV.',
	'images': [
		'https://lh3.googleusercontent.com/eM-IEKXhKUcNO_jyP0hGpkdI3av_9IbfBH0uDI_IgTLqM6FJfk0iJ7_Nk7_JsDzkVw604VvWtRLDre9hK38reBWPFnEhiuEYIj5r6vhAgADp2fl0lY4IKGy4iUfrz8V8',
	],
	'x': 50.0490652,
	'y': 14.4222808,
}, {
	'title': 'Ke Břvům',
	'x': 50.0668355,
	'y': 14.2598261,
}, {
	'title': 'Kilometrovka',
	'description': 'Krásná louka s pár stromama. Bohužel většinou tam je vysoká tráva, ale když ne, tak je to tam moc pěkný.',
	'x': 49.752627443315,
	'y': 13.36384750733,
}, {
	'title': 'Knejzlíkovy sady',
	'description': 'Bývalé sady, nyní pár mladých stromků. Polní cesta lemovaná stromky, "hříbky" na posezení, výhled na Přerov. Je to poměrně atraktivní lokalita, často se tam vyskytují výletníci a přes den i školy. Ale po ránu a přes oběd se to dá.',
	'x': 49.4825582,
	'y': 17.426548,
}, {
	'title': 'Kolíbka - Gabrielka',
	'youtube': [
		'71LlnTbe9gQ',
	],
	'x': 49.291053,
	'y': 15.058282,
}, {
	'title': 'Konopas',
	'description': 'Pekne startovaci misto na mid az long range polet. Da se dojet autem po polni ceste, ale to nedoporucuju. Lita se nad nedaleky les. Obcasny vyskyt chodcu.',
	'x': 50.153584728636,
	'y': 13.923920889589,
}, {
	'title': 'Kosená lúčka pri cyklotrase',
	'description': 'Kosená lúčka s bránkami bez sietí pri cyklotrase, je tam drevený altánok s lavicami a stolom, ideálny na rozloženie vecí, dá sa vymislieť trať pomedzi bránky. Najlepšie chodievať v skorých ranných hodinách (5:00-8:00)-bývajú tam neskôr psíčkari a potom môže byť altánok plný.',
	'x': 48.903161,
	'y': 18.039806,
}, {
	'title': 'Kostelíček',
	'description': 'Prima místo s polem okolo. Možnost branek nebo se jen tak vyřadit mezi stromy kolem kostela. Provoz minimální, hlavně pejskaři.',
	'x': 49.209807426387,
	'y': 16.705334186554,
}, {
	'title': 'Kostomlátky u koupaliste',
	'description': 'Bezva chládek.',
	'x': 50.1691402,
	'y': 14.9756366,
}, {
	'title': 'Kotelská park',
	'description': 'Menší park v Kotelské ulici. Jsou zde lavičky na posezení, strom uprostřed společně s navezenou hromadou zeminy a nějaké ty keře. Vedle vede stezka a občas projde nějaký ten človíček, ale je za keři a za stromy tak nehrozí nebezpečí. Mezi keři je i vidět, že někdo jde, tak je drobet i přehled jestli se zde někdo pohybuje. Ideální spíše pro freestyle a pro volné klidné zalítání na drobet rozmanitém místě.',
	'x': 49.728417085578,
	'y': 13.593874388571,
}, {
	'title': 'Krles',
	'description': 'louky nad Krásným Lesem u Ústí nad Labem',
	'x': 50.749076335397,
	'y': 13.967034816742,
}, {
	'title': 'Kudlovká louka',
	'description': 'Velká otevřená louka, s jedním stromem uprostřed a řídkým lesem ve kterém se dá skvěle létat.',
	'x': 49.205015324389,
	'y': 17.690048650797,
}, {
	'title': 'Kulíškova Školské ihrisko',
	'description': 'nechodiť počas školských hodín sú tam deti. Ku večeru sú tam ľudia čo behajú. Dobré na závody, otvorené pre verejnosť.',
	'x': 48.1504797,
	'y': 17.1308479,
}, {
	'title': 'Kunín kravín',
	'description': 'Pastvina od kravína, blízko cesty, zajímavé :)',
	'x': 49.638722093619,
	'y': 17.992377018725,
}, {
	'title': 'Kyjov',
	'description': 'long range s pekným výhľadom',
	'x': 49.209607117033,
	'y': 20.930887822729,
}, {
	'title': 'Lednice - Novy dvur',
	'description': 'Obrovsky prostor kde se nepohybuje moc lidi ani behem turisticke sezony. Da se prijet autem az na misto. Jsou zde stromy, posedy a dalsi atrakce...',
	'x': 48.7709695,
	'y': 16.8090928,
}, {
	'title': 'Lesik',
	'description': 'Maly lesik s "tunelom".',
	'youtube': [
		'zYiNeqHWwVE',
	],
	'x': 49.0434908,
	'y': 18.9283705,
}, {
	'title': 'Letisko Rusovce',
	'description': 'Velka otvorena plocha, vyborna pre zaciatocnikov (nie je do coho narazit), len pred letom vzdy treba skontrolovat ci funguje pipak, lebo po stranach letiska je pole zo zitom a ked kopta tam padne, tak ju bez pipaku najdes...<br>!!! chcel by som upozornit na letisko rusovce ze nieje moc vhodne,od letiska co vidite betonovy plot tak za nim trenuju kuklaci a ked trenuju specialne operacie vyrusia kompletne pásmo od 0Mhz az po najvyssiu moznu frekvenciu ak vam tam padne lietadlo tak aby ste vedeli ze co sa udialo!!!',
	'x': 48.0522332,
	'y': 17.1352682,
}, {
	'title': 'Letíště Hory',
	'description': 'Modelářské a UL letiště v krásném prostředí s nádherným výhledem na hory.',
	'x': 50.217668021511,
	'y': 12.775346780915,
}, {
	'title': 'Letiště Hradčany',
	'description': 'Opuštěné vojenské letiště, spusta opuštěných budov, žádní lidé...',
	'x': 50.6226227,
	'y': 14.7403908,
}, {
	'title': 'Letiště pro modeláře',
	'description': 'Spíše pro začínající',
	'x': 50.3965434,
	'y': 14.8906052,
}, {
	'title': 'Letiště Zábědov',
	'description': 'Nyní už nepoužívané polní letiště s betonovým povrchem. Povrch ideální pro přistávání RC letadel a zároveň velký volný vzdušný prostor pro FPV let jak letadly tak drony. Místo je neoplocené a volně přístupné',
	'x': 50.221253482109,
	'y': 15.45758437052,
}, {
	'title': 'Levice - Travnata plocha',
	'description': 'Sem tam strom, vela psov, ale da sa :) ludom sa to (zatial) paci',
	'x': 48.2054382,
	'y': 18.6019135,
}, {
	'title': 'Liberec-Harcov',
	'x': 50.7708737,
	'y': 15.0955904,
}, {
	'title': 'Liberec-Vesec',
	'description': 'Liberec-Vesec',
	'x': 50.7269581,
	'y': 15.0683767,
}, {
	'title': 'Lidice za hrbitovem',
	'description': 'Plocha je sekana, a jsou tu osamnely stromy i ridky hajek - Pozor v lete se tu opalujou lidi ale jinak je tu klid. Zadna vetsi akce neni vhodna ale pohodovy poletani jde.',
	'images': [
		'https://lh4.googleusercontent.com/IuQEZDuPPmZ2wf77VH0Q0D73_wwiy7ZjnF4ZOpI8uf1Fw9TidtSFR68qiyH8UfJ81aIfSVN6R6rnidBCDlTvXW8P8p2y319GPtxIzGi7RPiYfnJQC4RHtRMUOLRmZU0',
	],
	'x': 50.1374907,
	'y': 14.199636,
}, {
	'title': 'Lipka',
	'description': 'Místo mimo běžnou pozemní komunikaci, ale dá se všude dostat autem. Je to sice soukromý pozemek Kinských, ale nikdo nás nikdy nevyhodil i když okolo nás projíždí majitelé i několikrát denně. Jen nesmíme nechávat bordel. Ale to je asi takové všude.',
	'x': 50.157167582611,
	'y': 15.471787833884,
}, {
	'title': 'Lom',
	'x': 49.686304414393,
	'y': 13.468982350136,
}, {
	'title': 'Lom Homolák',
	'description': 'Starý opuštěný lom',
	'x': 49.902181125689,
	'y': 14.09065246582,
}, {
	'title': 'Lom u Ochozu',
	'description': 'Stary lom u Ochozu. Obcas se objevi nejaky cyklista/pejskar jinak celkem klid. Podlozi celkem tvrde takze branky/vlajky jdou trochu z tuha ale jde to.',
	'x': 49.254977893702,
	'y': 16.755677698224,
}, {
	'title': 'Lom za Hády',
	'description': 'Ideál na long range plachtění',
	'youtube': [
		'2o5tJI0heJg',
	],
	'x': 49.224376163711,
	'y': 16.698233252518,
}, {
	'title': 'Louka a slalom',
	'description': 'Velká louka s pár stromy na slalom a létání mezi nimi',
	'x': 50.377720095036,
	'y': 16.161565941591,
}, {
	'title': 'Louka Dobříšská',
	'description': 'Zde jsem začínal již před sedmi lety s R/C větroněm a před půl rokem i s FPV. Myslím si, že pro začátek je to dobré místo, ale časem to bude chtít i nějaké ty překážky.',
	'x': 49.6942262,
	'y': 14.0180424,
}, {
	'title': 'Louka mezi Kyšicemi a Červeným Hrádkem',
	'description': 'Docela sympatická louka na zalétání. Je tady velkej volnej prostor daleko od všeho a všech a když sem tam byl lítat, tak tam kromě kolegů pilotů nebyl nikdo, takže se neni moc čeho obávat. Bohužel před sluncem se tady nikam neschováš, protože kromě stromů podél silnice tam nic neni a vlajku co si nedoneseš nemáš.',
	'x': 49.755888375155,
	'y': 13.471122054383,
}, {
	'title': 'Louka mezi Podhájím a Radobyčicemi',
	'description': 'Krásná louka 2 stromy a jeden kopeček s kanálem, plno místa na test i na trať',
	'x': 49.694370118955,
	'y': 13.403311208327,
}, {
	'title': 'Louka na Plzeňské',
	'description': 'Jde o pozemek města určený k volnému pobíhání psů, ale zažil jsem zde využívat pejskaře pozemek jen jednou a i tak jsem létal, protože pozemek je veliký a každý byl na jedné straně. Kolem vede cyklostezka směr na Klabavu, kde občas jezdí cyklisti a prochází lidé a bruslaři, kteří se rádi kouknou na koníček s dronem (dobrá prezentace). Na kraji se dá dobře zaparkovat.',
	'x': 49.742887020069,
	'y': 13.57119848839,
}, {
	'title': 'Louka na podlesí',
	'description': 'spousta prostoru a velká louka na zkoušení nových modelů případně freestyle ve výškách',
	'x': 49.2401891,
	'y': 17.6772165,
}, {
	'title': 'Louka na přehradě',
	'description': 'Provoz hodně velký, ale najdou se i chvíle, kdy je prázdno. Pozor, pejskaři a kolemjdoucí chodí i přímo přes pole a usazují se na betonových kanálech nebo přímo na louce. Celkem často zde ale potkáte nějakou jinou poletuchu, lidé jsou celkem zvyklí (startují tu i balony).',
	'x': 49.2280116,
	'y': 16.5185371,
}, {
	'title': 'Louka Nový Knín',
	'description': 'Členitá louka ve svahu s přirozenou velkou bránou mezi dvěma stromy (tunel). Každá okolní louka (a že jich tu je) obsahuje různé prvky pro průlety, ale spojuje je nedostatek civilistů a tedy pocit sucha, klidu a bezpečí...',
	'x': 49.7779802,
	'y': 14.2855954,
}, {
	'title': 'Louka s pár stromky',
	'description': 'Dobré na blbnutí s menší koptérou 3" ideál. Lidi tudy moc nechodí akdyž ano tak po pěšině která vede prostředkem louky. Dá se tu postavit trať.',
	'x': 50.2325736,
	'y': 14.2942402,
}, {
	'title': 'Louka Tesco KrPole',
	'description': 'Provoz různý, většinou pejskaři. Nutno dávat na ně pozor. Branky nevím jestli tu kluci lítali, ale dá se využít keřů a stromků.',
	'x': 49.2164198,
	'y': 16.6096652,
}, {
	'title': 'Louka Tesco KrPole II',
	'description': 'Dobré místo s "přírodními" překážkami (keře, stromky, železné tyče) na technickou trať asi pro 1 nebo 2 lidi. Provoz minimální a je to celkem přehledné místo.',
	'youtube': [
		'k80KUYrquG8',
	],
	'x': 49.219328,
	'y': 16.6069937,
}, {
	'title': 'Louka u cyklostezky',
	'description': 'Rozsáhlé, rozdělené 3 louky, které dělí řada stromů, kde se dá velmi dobře vyblbnout. Na místě vás nikdo neotravuje, kromě hmyzu. Místo je na konci cyklostezky před paskovskými rybníky. Tohle místo mám velmi rád, protože je schované vedle cyklostezky a je tam božský klid.',
	'x': 49.756711153738,
	'y': 18.278389921311,
}, {
	'title': 'Louka u Hlušovic',
	'description': 'Kousek od oblíbené cyklostezky z Černovíra do Hlušovic. Cesta je dostatečně daleko, aby se cyklisti necítili ohrožení. Prostorná louka na branky, pár stromů a keřů jako přirozené překážky. Většinou je tu na létání klid. Cyklisti občas zdálky komentují vaše snažení :-)',
	'x': 49.633790697531,
	'y': 17.261382550255,
}, {
	'title': 'Louka u Kotlerova',
	'description': 'Velká louka na lítaní s coptérou ale i s letadlem. Často posekaná. Na louce je jen pár velkých vzrostlých stromů. Neparkovat na trávě!!!',
	'x': 49.717150993708,
	'y': 13.423285033311,
}, {
	'title': 'Louka u Krimova',
	'description': 'Menší louka, dá se v pohodě přejít i na větší, hned u silnice možnost zaparkovat 2-3 auta. Často posekáno, žádné dráty',
	'x': 50.481761421443,
	'y': 13.330556531726,
}, {
	'title': 'Louka u obce Drásov',
	'description': 'Poměrně daleko od civilizace, za to velmi pěkné místo.',
	'x': 49.695028984972,
	'y': 14.125673366911,
}, {
	'title': 'Louka u vodojemu',
	'description': 'Ideální louka na stavbu branek. Alej stromů bonusem. Na místo se dá zajet autem, zalidněnost: minimální.',
	'x': 50.1464501,
	'y': 14.3581867,
}, {
	'title': 'Louka v Kladně',
	'description': 'Krásný obří pozemek, kousek od paneláků, super na pár baterek',
	'x': 50.12703972224,
	'y': 14.128698108398,
}, {
	'title': 'Louka v lese',
	'description': 'prijemna louka pro polet, skoro zde nefouka. moc pekny les.',
	'x': 49.222312864008,
	'y': 16.821512865271,
}, {
	'title': 'Louka za Č.B.',
	'description': 'Většinou posekaná louka. Poměrně klidné místo, po úzké asfaltce kolem občas něco projede.',
	'x': 48.999405,
	'y': 14.5087606,
}, {
	'title': 'Louka za fotbalovým stadionem',
	'description': 'Velká louka, vede tudy nějaká naučná stezka ale nikdy jsem tam nikoho nepotkal',
	'x': 48.9595387,
	'y': 17.3643422,
}, {
	'title': 'Louka za Globusem',
	'description': 'Dobrá louka na uklidnění, nikdo tu není, často posekáno a jeden pěkný strom, zaparkovat se dá u vjezdu na louku',
	'x': 50.456621433886,
	'y': 13.384424627463,
}, {
	'title': 'Louka za Klajdou u lesního lomu',
	'description': 'Provoz velmi malý, téměř žádný (pejskaři nebo turisti spíš chodí bokem), branky bez problémů.',
	'x': 49.2254734,
	'y': 16.6928673,
}, {
	'title': 'Luka',
	'description': 'Sekana plocha pro let. modelare, nova asfaltka, misty pejskari, vetsinou jen zvedavci :)',
	'x': 49.571171762022,
	'y': 13.340244462581,
}, {
	'title': 'Lúka medzi Závažnou Porubou a Ilanovom',
	'description': 'Lúka medzi Závažnou Porubou a Ilanovom. Zaparkovať sa dá pri ceste alebo na polnej ceste.',
	'youtube': [
		'8FwvCeYeyXo',
	],
	'x': 49,
	'y': 19,
}, {
	'title': 'Lúka za mestom',
	'x': 49.101222532919,
	'y': 21.111001277115,
}, {
	'title': 'Luka za popradom',
	'description': 'Na tuto luku chodievam obcas lietat, je to pekna velka plocha. Je to medzi cyklo chodnikom a cestou tak treba davat velky pozor na ludi aj auta. Niekedy chodim az za cyklo chodink ku zhluku stromov - problem je ze je tam mociar :)',
	'x': 49.0366371,
	'y': 20.269593,
}, {
	'title': 'Luke\'s Home',
	'description': 'Obrovská louka',
	'x': 49.889809910397,
	'y': 16.917955416389,
}, {
	'title': 'LV Legal a.k.a. Pipe Dream',
	'description': 'Da sa tu vyblázniť, sem tam prejde vlak. Pozor však na el. vedenie nad traťou !!!',
	'youtube': [
		'ikB79qAO-ug',
	],
	'x': 48.2044068,
	'y': 18.5961092,
}, {
	'title': 'Lyžařský vlek Radvanice',
	'description': 'Domovský spot. Možnost umístění vlajek, bran + přírodní trať Členitý terén (kdo by to čekal na sjezdovce že? :-))',
	'x': 50.568601424323,
	'y': 16.043354272842,
}, {
	'title': 'Machnín',
	'description': 'Louky, stromy řady, jednotlivé stromy, les. Každý si najde svoje.',
	'x': 50.798408200378,
	'y': 14.97864704467,
}, {
	'title': 'Malá Louka',
	'description': 'Malá louka s pár stromama, málo používaná příjezdová cesta, pár lamp, dá se tam dělat zábavný freestyle, tráva je relativně nízká takže najít koptéru není problém',
	'x': 49.824140347549,
	'y': 18.439701112532,
}, {
	'title': 'Malá louka u university',
	'description': 'Ideální na test quadu, celkem dost místa',
	'x': 49.727582471001,
	'y': 13.356648653859,
}, {
	'title': 'Malenovice Riviéra',
	'description': 'Zapomenuté koupaliště, budova, tobogány.<br>Je tam cedule zákaz vstupu, na větší akci to není.',
	'x': 49.2007261,
	'y': 17.6007521,
}, {
	'title': 'Malenovice skleníky',
	'description': 'Opuštěné budovy, skleníky, komíny veřejný ale soukromý pozemek, v neděli jsou tam militaristi.',
	'x': 49.2092412,
	'y': 17.5866035,
}, {
	'title': 'Malešice',
	'description': 'Jedno z nejpopulárnějších míst v Praze. Pozor na horší příjezdovou cestu.',
	'youtube': [
		'yLc9ayPiiKE',
	],
	'x': 50.0903013,
	'y': 14.5243183,
}, {
	'title': 'Malý park',
	'description': 'Jde o maličký park, nebo spíš jen plácek se třemi lavičkami. Na přilehlém chodníku chodí dost lidí, ale přímo přes lavičky prakticky nikdo. Doporučuji pro menší drony. 5" je na hranici, ale na pomalé létání to ještě jde. Kvalitní spotter je zde nutností',
	'x': 50.073209677212,
	'y': 14.380676900261,
}, {
	'title': 'Martin',
	'youtube': [
		'gKLdejSZO3w',
	],
	'x': 49.0210412,
	'y': 19.0348005,
}, {
	'title': 'MattyStuntz world',
	'description': 'Supr na trénování Mattystuntz triků.',
	'youtube': [
		'c2EfY-SnfC8',
	],
	'x': 50.4596351,
	'y': 16.0483539,
}, {
	'title': 'Medlaňák',
	'description': 'spot přímo pod kopcem s parkovištěm. Spousta mladých vysázených stromků poslouží jako překážková a slalomová dráha.',
	'x': 49.2358144,
	'y': 16.5692496,
}, {
	'title': 'Medlánky Bouldering park',
	'description': 'Provoz je tady velmi rušný, ale ve večerních hodinách bývá park téměř prázdný.',
	'x': 49.2285773,
	'y': 16.5721035,
}, {
	'title': 'Městské sady opava',
	'x': 49.953109162786,
	'y': 17.88584630271,
}, {
	'title': 'Městský stadion',
	'description': 'Multifunkční stadion, tráva pravidelně udržovaná - ideální na stavbu tratě. Příjezd buď z hlavní silnice nebo zadem po štěrkové cestě (doporučuji). Liduprázdno, maximálně Vám budou fandit důchodci z nedalekého DD.',
	'x': 50.4021684,
	'y': 14.0333444,
}, {
	'title': 'Milíčovský vrch',
	'description': 'Hodně zajímavých stromků, docela málo místa a občas někdo projde kolem a kouká se. Chce to buzzer a taky armování pod úhlem na sundavání ze stromu :D. Parkování z ulice Proutěná. 3pejskaři/hod..',
	'youtube': [
		'5jaUyYKoRjI',
	],
	'x': 50.0238436,
	'y': 14.5318437,
}, {
	'title': 'Milska stran',
	'description': 'Vhodne misto na kratky polet na protejsi Milske strani.',
	'x': 50.228232571042,
	'y': 13.876203730964,
}, {
	'title': 'Místečko na kraji Rokycan směr Veselá',
	'description': 'Volný plácek na kraji Rokycan směrem na Volduchy. Jednou jsem zde viděl začínajícího letce. Místo spíše pro branky.',
	'x': 49.722084685963,
	'y': 13.603549750711,
}, {
	'title': 'Mistni modelarske letiste',
	'description': 'udrzovana mensi pristavaci draha pro modely',
	'x': 49.178890763993,
	'y': 16.871121221596,
}, {
	'title': 'Model. letiště Sobínka',
	'description': '<a href="http://www.sobinka.eu" target="_blank">www.sobinka.eu</a>',
	'x': 50.0549207,
	'y': 14.2701137,
}, {
	'title': 'Modelářská plocha u letiště',
	'description': 'Jedná se o modelářskou plochu, která se součástí místního aeroklubového letiště. Protože se tam občas létá i s velkými modely, tak se pak musíme dohodnout. Protože je na letišti docela provoz (hlavně ultralighty), striktně se dodržují pravidla a nestrpí se žádné vylomeniny. Plocha se udržuje, platí se poplatky. Pokud je zájem tam létat, stačí se tam stavit, na ceduli je kontakt.',
	'youtube': [
		'MWgh3H2jGVQ',
	],
	'x': 49.2869833,
	'y': 17.4179528,
}, {
	'title': 'Modelářské letiště',
	'description': 'Hodně velkého a volného prostoru.',
	'x': 49.299544786971,
	'y': 17.65062132942,
}, {
	'title': 'Modelářské letiště Vlachovo Březí',
	'description': 'Seká se tam, nikdo nic nechce platit. Je tam i pár stromů.',
	'x': 49.083205532469,
	'y': 13.977295661337,
}, {
	'title': 'Mosovsky park',
	'youtube': [
		'RnHkN1fnfj0',
	],
	'x': 48.9144338,
	'y': 18.8930941,
}, {
	'title': 'Most',
	'description': 'Spousta sloupů pryč od lidí, dobré místo ale spíš vhodnější pro tiny whoop nebo menší koptéry',
	'x': 49.806613567681,
	'y': 18.204320668935,
}, {
	'title': 'Most na Šebík',
	'description': 'Krásná obří plocha, dá se blbnout mezi sloupy, mezi stromy, sem tam se zde objeví pejskaři',
	'x': 50.467502431429,
	'y': 13.374700626576,
}, {
	'title': 'Most Zbraslav',
	'description': 'Super místo kde nejsou moc lidi je to v průmyslové zóně. Dá se přijet autem kamkoli pod most z ulice Výpadová.',
	'images': [
		'https://lh5.googleusercontent.com/SRZxYUPtnfdGRFR9LjyDYFAq3Wwml3wWEWR_QrVU3Gq6M5WGXoBLjuDdpOV4LHLxtXs-O7nowjT8zRJmmzVkw5Hc5NjBzVniJHMfP0WHgSCTax_WB3ZQRrLL4ZwQ7RQ',
	],
	'x': 49.9920842,
	'y': 14.3781853,
}, {
	'title': 'Mosty a hrad',
	'description': 'Cestný most. ktorý je vo výstavbe. Keďže je výstavba skoro zastavená, tak tam skoro nikoho nenájdete, okrem turistov, ktorí kráčajú na hrad (minimálny počet). Pod mostom sa nachádza dlhý drevený tunel pre turistov, cez ktorý sa dá lietať. V okolí sa nachádzajú ďalšie takéto mosty a aj hrad Likava. Mimo sezóny nie je problém lietať ani na hrade. Pozor, aby vám tam nezostal dron, lebo vstup na hrad je už uzavretý!',
	'youtube': [
		'a2Z5RTr-MAY',
	],
	'x': 49.103606285371,
	'y': 19.308607760188,
}, {
	'title': 'Motokrossová trať - Kostěnice',
	'images': [
		'https://lh3.googleusercontent.com/ylz3kx8fxuz1NIu7YXxzRcjA7s-mv7mPRjQr25VS-NFYWd_PANCTEWiMihrB-nxuV8-NXc_q_AnvdVjO4B8X9siOrIewC72WsE-hfjtXmPBL3oii-baDCRZlGmQQBMwK',
	],
	'x': 50.0162056,
	'y': 15.9022486,
}, {
	'title': 'Muráň castle',
	'youtube': [
		'O6hp5Y3qjy0',
	],
	'x': 48.745493,
	'y': 20.0558853,
}, {
	'title': 'Muzeum na demarkační linii',
	'description': 'Muzeum na demarkační linii. S vedoucím muzea jsem se bavil a létání s dronem mu nevadí. Je to fajn chlápek. Létat ale jen nad tankodromem, vystaveným tankem v popředí a nad přilehlým parkovištěm. Nelétat přímo nad muzeem !!Pestrá krajinka pro freestyle - stromy, průlety mezi stromy atd, vystavený tank v popředí (pro dron nezničitelný pancíř), okruh tankodromu se stromem uprostřed a mnoho dalšího co si freestyleři určitě oblíbí.',
	'x': 49.727559668756,
	'y': 13.587292324119,
}, {
	'title': 'Na Komonec',
	'description': 'Startovaci misto pro long range na blizke kopce jako Brda nebo Komonec. Da se letet i pres to udoli napravo a zase zpet pripadne mezi stromy sadu.',
	'x': 49.136342514521,
	'y': 17.754430111251,
}, {
	'title': 'Na Srdov',
	'description': 'Vhodne misto pro vzlet kolem vyhasle sopeky Srdov. Pozor je to vyletni misto, ale pokud jsou na kopci lidi, tak jdou snadno spatrit.',
	'x': 50.412844755589,
	'y': 13.830685617478,
}, {
	'title': 'Na vesláku',
	'description': 'Louky',
	'x': 50.1860974,
	'y': 15.0557542,
}, {
	'title': 'Nad Pivčákem',
	'description': 'Fajn místo na polet v Rumburku. Bohužel však málo přírodních překážek a hned vedle hustý les.',
	'x': 50.95629608036,
	'y': 14.564768026903,
}, {
	'title': 'Nebušická louka',
	'description': 'skvělé místo naprosto bez lidí, jen pozor na el. sloupy na konci louky',
	'x': 50.1105337,
	'y': 14.3515348,
}, {
	'title': 'Největší krtinec ve střední evropě',
	'description': 'Neoficiální motokrosová dráha - není to tak uplně louka, ale motokrosáci tu nejezdí moc často a bývají tak 3 nebo 4. Na branky to asi moc není, ale dá se kolem stromů a keřů. Provoz téměř nulový pokud zde nejsou motokrosaři. Ale když jsem tam byl s nima tak neměli problém, když jsem lítal zároveň v dostatečné vzdáleností od jezdce.',
	'x': 49.1683418,
	'y': 16.6705835,
}, {
	'title': 'Nepoužívaná železnice',
	'description': 'Zarostlé koleje po kterých nejezdí nikdy žádné vlaky. Parádní na high speed runy zarostlým koridorem.',
	'youtube': [
		'jtYzrh8YhBE',
	],
	'x': 50.2436543,
	'y': 14.3586373,
}, {
	'title': 'Nod Okružní',
	'description': 'Místo vhodné spíš na blbnutí s micro-quady. Se svou Vendettou bych sem nešel. Je tu tráva, stromky jako přirozené překážky - fajn na rychlé vyblbnutí. Ale chodí tu mudlové na procházky a pejskaři, takže je lepší mít někoho s sebou na kontrolu terénu.',
	'x': 49.590645870084,
	'y': 17.223594821218,
}, {
	'title': 'Nová paseka u Žalmanova kamene',
	'description': 'O vikendu na vychazce s psama sme kousek od Vitovic objevili novej "placek"... dela se pomerne velka prorezavka, tak po tom je tahle paseka..',
	'youtube': [
		'_b8ONkR3dZE',
	],
	'x': 49.236395,
	'y': 16.82051,
}, {
	'title': 'Opustena budova',
	'description': 'volne pristupne (zadny plot), idealni "bando" pro zacatecniky co si chteji vyzkouset zalitat v uzavrenych prostorach protoze mista je tam spousta a "diry" hodne velke. schodiste jsou zbourany takze pokud by koptera zustala viset na hornim patre nebo na strese tak bez zebriku se tam neda dostat (pro tento pripad doporucuji mit nastaven anti-turtle mode). a jeste jako bonus hned kousek vedle je famozni Panska Skala (Varhany) z filmu Pysna Princezna (kdyz se postesti a nejsou okolo turisti).',
	'youtube': [
		'z9WVuhGWsf4',
	],
	'x': 50.7733739,
	'y': 14.4851813,
}, {
	'title': 'Opuštěná farma',
	'description': 'Ideální na fpv, mnoho oken a rozbořených staveb. U cesty je však závora - dá se lítat od ní nebo vlézt na pozemek, který je však soukromý. Byl jsem tam ale několikrát a bez problému.',
	'x': 50.4283834,
	'y': 14.0717483,
}, {
	'title': 'Opuštená farma',
	'description': 'Velký rozpadlý statek, ideální na fpv. Dá se zajet po polní cestě až přímo dovnitř areálu. Je tam velké množství průletů zdmi, oblouky atd.',
	'x': 50.315791229348,
	'y': 14.471488913434,
}, {
	'title': 'Opustena Korytnica',
	'description': 'Bandoooo, urbex, TW heaven',
	'x': 48.8886598,
	'y': 19.2841548,
}, {
	'title': 'Opusteny hotel',
	'youtube': [
		'jU5yU-gyzuY',
	],
	'x': 49.0821937,
	'y': 18.8675272,
}, {
	'title': 'Opuštěný vápencový lom',
	'description': 'Dá se startovat z horní hrany (označený bod), odkud je rozhled na celou jámu. Na dně rostou náletové dřeviny, ale dá se mezi nima ptokličkovat. Na dno se dá dostat tajnou cestou, nebo přes recyklační závod. Vstup oficiálně zakázán.',
	'x': 49.4810142,
	'y': 17.4198103,
}, {
	'title': 'Ostrava Hrušov',
	'description': 'Dostatečně velký prostor s nízkou trávou, pěknými stromy, ale i s nebezpečným vedením. Parkování 100m od místa.',
	'x': 49.865562077158,
	'y': 18.279878393945,
}, {
	'title': 'Ostrava Privoz - Prednadrazi',
	'description': 'Byvale obytne domy s pohnutou vice nez <a href="https://cs.wikipedia.org/wiki/P%C5%99edn%C3%A1dra%C5%BE%C3%AD_(Ostrava)" target="_blank">stoletou historii</a>. Prijezd a parkovani: Z ulice Na Naspu. Na co bacha: mezi domy jsou natazeny zbytky el. vedeni. Pred letanim si peclive zkontroluj a projdi prostor. Lokalita je opustena, ale oblibena mezi milovniky opustenych mist, takze pozor i na lidi. Derave domy bez stropu a strechy bez krytiny lakaji k pruletum, ale spise nedoporucuji. V pripade padu je zachrana stroje velmi riskantni.',
	'x': 49.850228747106,
	'y': 18.258641660213,
}, {
	'title': 'Pakov Motokros',
	'description': 'POZOR!!! Letej jen, kdyz se nejezdi! Na miste je obvykle stale hlidac. Zajdi za nim, pozdrav, slusne pozadej. Mas-li pro nej cigaretku, urcite to pomuze.',
	'x': 49.74345184248,
	'y': 18.285707831383,
}, {
	'title': 'Památná Lipová Alej - Dráchov',
	'youtube': [
		'oeRo0oTNMtM',
	],
	'x': 49.322359,
	'y': 15.071651,
}, {
	'title': 'Park',
	'description': 'Velky udrziavany park s vela prekazkami. vynikajuce miesto, sem tam sa ukaze nejaky clovek...',
	'x': 48.3849969,
	'y': 17.577492,
}, {
	'title': 'Park Drama Věků',
	'description': 'Park na vesnici, většinou zcela bez lidí. Řídce vysazené stromky a uhlazené pískové cestičky se mísí s udržovanou travnatou plochou a dětskými prolézačkami.',
	'youtube': [
		'hAERUySYxR4',
	],
	'x': 49.672329873568,
	'y': 14.501325700096,
}, {
	'title': 'Park klíčov',
	'description': 'Malý park, který je ideální pro tréning precizní práce.',
	'x': 50.115976773431,
	'y': 14.515147066275,
}, {
	'title': 'Park Komjatice',
	'description': 'Najlepšie chodiť ráno, chodí najmenej civilistov (overené cez víkendy).',
	'youtube': [
		'nPWyYjZUpag',
	],
	'x': 48.1595007,
	'y': 18.1829416,
}, {
	'title': 'Park Palárikovo',
	'description': 'Najlepšie chodiť ráno, chodí minimum civilistov.(overené cez víkendy)',
	'x': 48.0369677,
	'y': 18.0680519,
}, {
	'title': 'Park u Čepkova',
	'description': 'park přímo v centru města, kde nechodí po travnaté ploše vůbec nikdo. Po chodníku k zastávce už chodí více chodců. spousta stromů je zde na vyblbnutí.',
	'youtube': [
		'PE-JF6bYXhg',
	],
	'x': 49.2285248,
	'y': 17.6585376,
}, {
	'title': 'Park u Hostivařské přehrady',
	'description': 'Fakt velký prostor, kde jsou i shluky stromu pod kterými se dá prolétavat. Je tam i jakýsi přístřešek přes který lze lítat taky. Když jsem tam byl já tak tam bylo fakt málo lídí. Sem tam projede nejaký cyklista nebo někdo jde se psy.',
	'x': 50.045492516308,
	'y': 14.534469781108,
}, {
	'title': 'Park Zličín',
	'description': 'Takřka opuštěný park - skvělé místo na létání s přírodními překážkami',
	'x': 50.0550998,
	'y': 14.3034053,
}, {
	'title': 'Parking Hostivice',
	'description': 'Za rozbitou bránou louka, občas sekaná městem. Nelítat nad auty a pozor na pejskaře. Lze nabíjet z auta.',
	'images': [
		'https://lh3.googleusercontent.com/9MWAkvM-pxDY-eQkA4ARtP6RCleyaVMy8-rFPimdBBmVmXhS_WkyCPXDW3z08L2yxNlfMlHCNdbAuDhmd-xbY2yk03gp_mwIES-rBidS15VO0QpH3uhRff5LNfFaZ_ul',
	],
	'x': 50.0834116,
	'y': 14.2453033,
}, {
	'title': 'Paseka u cementary',
	'description': 'Vykacene misto s par samostatne stojicimi stromy',
	'x': 49.227579566395,
	'y': 16.77971219539,
}, {
	'title': 'Pekný kopec zo zrúcaniou hradu',
	'x': 49.195628060463,
	'y': 20.970647368787,
}, {
	'title': 'Plochá dráha',
	'description': 'Jedná se o bývalou plochou dráhu, že které se udělalo hřiště, dá se tady super zalitat ale spíše ráno nebo večer, kdy tu není moc lidí',
	'x': 50.467466606641,
	'y': 13.392501709527,
}, {
	'title': 'Ploučnická vyhlídka',
	'description': 'Sice do kopce ale pole pecka.',
	'x': 50.73623907329,
	'y': 14.307274920846,
}, {
	'title': 'Pod Hády',
	'description': 'Placek kde nikdo moc nechodí víc keřů ale vlajka branka asi bez problémů.',
	'x': 49.2138898,
	'y': 16.6644868,
}, {
	'title': 'Pod Hrází Hostivice',
	'description': 'Staré fotbalové hřiště, často sekaná travnatá plocha bez stromů, vhodné na závody. Pohyb osob nutný spoter. Lze nabíjet z auta.',
	'images': [
		'https://lh5.googleusercontent.com/7RY4nBxY-M4BEenaDGXmyDggORVoSobpEFz0s9Xb4YcDV_LQWfyKspxlNdMw4F8w_8WHx91x81bLJY11xhJdhuZu7B1JSOq4AcFotyARjKCOrgGvEbX4SNNQ8956_6zs',
	],
	'x': 50.0771068,
	'y': 14.2431763,
}, {
	'title': 'Pod Mostem',
	'description': 'Místo bez lidí, spousta sloupků a menší pole s pár stromama. Blízko elektrárny a nad mostem je dálnice takže hlavně nelétat nějak výš.',
	'x': 49.826935121313,
	'y': 18.215770216736,
}, {
	'title': 'Pod mostem',
	'description': 'Dá se dostat autem úplně pod most nové rychlostní spojky Opava-Ostrava, cestou z leva (z prava jde lesem) od výjezdu z Hrabyně na Ostravu, je to štěrk a makadam, ne úplně pro sport auto... dole jsou pilíře a vyvýšené roury kanalizace vhodné pro kličkování a navigaci.',
	'x': 49.8841451,
	'y': 18.0680788,
}, {
	'title': 'Pod mostem Na Hůrce',
	'description': 'Dobré místo na vybití baterek, když prší.',
	'images': [
		'https://lh5.googleusercontent.com/kFKRXrieclqNSqrncqa0R7-jYYPnP9htxM3MuQvArTjv577H_Gtl9VHD-71xnZdpX5VAXNXt8FuGWgcjQjD-2W_xTvkiUSSmLnwpb9FHcCX9lWP0yQFuU2Z22jlyL5As',
		'https://lh5.googleusercontent.com/WryEUpAI-A4voTbYC52ku3AmQi9XWW3039T7rT66-n7IMNJg58awS2cGPx_FWsSu5AHmDI1nfnUivZSKiuF9PhjYXIDz9FuesUKQu0TL0XUn8IQQ5l023WgZXsxrH_vo',
	],
	'x': 50.0779985,
	'y': 14.2926389,
}, {
	'title': 'Podhurka',
	'description': 'Louka nedaleko Podhorskeho rybnika. Dostatek prostoru. Obcasna pritomnost chodcu. Zastresene posezeni. Dostatek objektu k radeni. Moznost pro delsi vylet s kopterou.',
	'x': 50.144007606274,
	'y': 13.892923205248,
}, {
	'title': 'Podolí-alej stromů,louka',
	'description': 'Alej pěkně rostených stromů pro freestyle, hned vedle velká louka pro racetrack',
	'x': 49.434324683479,
	'y': 17.85761113916,
}, {
	'title': 'Podzámka',
	'description': 'spousta louk se stromy a jezirky :) park :)',
	'x': 49.3049509,
	'y': 17.3918724,
}, {
	'title': 'Pohoda na cyklostezce u Ostravice',
	'description': 'Rozsáhlé místo na to pohodové polétáníčko. Spousta stromů a překážek. Místem prochází velmi málo lidí, téměř nikdo. Na místě se nachází nově zasazené stromy, které až dorostou, posunou místo na vyšší úroveň.',
	'x': 49.813025526788,
	'y': 18.289173543809,
}, {
	'title': 'Pohřebiště pražců',
	'description': 'Neoplocený, nehlídaný prostor, kde je naveženo spoustu betonových panelů s železničními pražci. Spousta překážek pro freestyle. Dá se pohodlně zaparkovat na konci ulice Sazečská, policajti se tam občas jezdí zašívat, ale žádný zákaz tam není.',
	'images': [
		'https://live.rotorama.cz/wp-content/uploads/2021/07/IMG_5176-1024x768.jpg',
	],
	'x': 50.069667,
	'y': 14.522639,
}, {
	'title': 'Pole',
	'description': 'Popici pole, dobrý na longe range a freestyle.',
	'x': 49.02387177735,
	'y': 15.103768107693,
}, {
	'title': 'Pole a louka u Butovic',
	'description': 'Autem přístupná louka i pole vedle záchranné stanice pro zvířata, vždy bez problému, dá se létat i z auta. Před políčkem jsou i stromky a cestičky, na freestyle i volné polétání ideál :) vedle chodí občas cvičit psy, ale dá se létat mimo',
	'x': 50.047651533966,
	'y': 14.370019781115,
}, {
	'title': 'Pole na Bartošce',
	'description': 'Velký prostor nad loukou u splavu řeky dřevnice hned vedle cyklostezky.',
	'x': 49.224086,
	'y': 17.7085608,
}, {
	'title': 'Pole u Chocerad',
	'description': 'Velké pole bez překážek, je zde prostor pro zaparkování a betonové desky, ze kterých se dobře startuje. Odlehlé, ale bez lidí...',
	'x': 49.876775162853,
	'y': 14.809219727937,
}, {
	'title': 'Polňačka se stromy',
	'description': 'Polňačka se stromy s dobrými rozestupy a dostatečnou výškou větví. Ideální na první krůčky s freestyle.',
	'x': 49.4711256,
	'y': 17.4908245,
}, {
	'title': 'Poľovnícka chata - Horná Kráľová',
	'description': 'Ideálne na trening. Bazové kríky nízke stromy s tenšími konármi + vysoká tráva. Koptéra v pohode prežije aj tvrdší pád. Chodievame sem lietať podvečer a samozrejme sa snažíme rešpektovať prípadných poľovníkov. Oni zasa vedia pochopiť nás :).',
	'youtube': [
		'1T-aWkliujU',
	],
	'x': 48.2280462,
	'y': 17.8782856,
}, {
	'title': 'Popovická Louka - Letná',
	'x': 50.770367747653,
	'y': 14.18053425787,
}, {
	'title': 'Posezení pod kotlem',
	'description': 'Příjemný koutek pod kotlem, kde je lavička se stolkem a odpaďákem. Jedná se o jakési rozcestí zapadlé na kraji lesa, kde sice občas (hodně málokdy) projde nějaký chodec, ale je zde pád míst pro průlety (informační tabule, závora, mezi stromy) a rovnou i průlet na otevřená pole. Dá se na místo zajet autem (cesta je ale nerovná a kamenitá a musí se pomalu)',
	'x': 49.721688556602,
	'y': 13.592978475288,
}, {
	'title': 'Postřelmov - Soutok',
	'description': 'Velká louka se stromy',
	'x': 49.9089045,
	'y': 16.9292557,
}, {
	'title': 'Potrubia',
	'description': 'Kludne miesto, vecsinou bez ludi, da sa rozbalit veci na betonovom zaklade sachty. V okoli je niekolko nizkych objektov, ale hlavna atrakcia je potrubia, ktore su vyvedene z elektrarny. Parkovat sa da na ulici Heldova.',
	'x': 50.086023045165,
	'y': 14.522725614364,
}, {
	'title': 'Pri Gajarskej štrkovni',
	'description': 'Miesto je dosť ďaleko od obývanej oblasti. Dá sa pekne polietat popod cyklo most. Pod mostom tečie mala rieka.',
	'youtube': [
		'vjlF5Cfc9dA'
	],
	'x': 48.4972772,
	'y': 16.9292235,
}, {
	'title': 'Pri štrku',
	'description': 'Pekné rozloženie stromov do kruhu, dobre sa lieta medzi nimi a v tuneloch z listov a stromov. Pozor, je to pri vode, jedného som tam utopil :(',
	'x': 48.1155187,
	'y': 17.1388328,
}, {
	'title': 'Pri Vrábloch',
	'youtube': [
		'rbiFZHZw_ZI',
	],
	'x': 48.2161305,
	'y': 18.2988882,
}, {
	'title': 'Přílucká vyhlídka',
	'description': 'spousta prostoru, všude pole a žádní lidé',
	'x': 49.2347916,
	'y': 17.7114844,
}, {
	'title': 'Prokopské údolí',
	'description': 'Příjemná louka nad prokopským údolím, dostupná pěšky nebo lépe na kole.',
	'x': 50.042995727164,
	'y': 14.36872811837,
}, {
	'title': 'Prokopské údolí - nad skalami',
	'description': 'Pěkná louka a zároveň mírné skály nad kterými se dá dobře létat. Přístupné pěšky nebo na kole.',
	'x': 50.039935164781,
	'y': 14.358707623181,
}, {
	'title': 'Prosek za benzínkou',
	'description': 'mensí prostor za benzínkou na proseku. par stromů.',
	'x': 50.119299847193,
	'y': 14.50299490022,
}, {
	'title': 'Průhonice/Rozkoš',
	'description': 'Hezká rovná louka, stromky kolem cyklostezky, po které ale moc lidí nechodí.',
	'x': 49.9934775,
	'y': 14.5373583,
}, {
	'title': 'Quadova draha s pieskom',
	'description': 'Vela stromov a pieskovom terenu, idealne na trening klickovania medzi prekazkami a sledovani quadovych tras. Vecinou je to prazdne, a keby nahodou nebolo, skoro iste sa tu najde nejaky volny kut pre kazdeho. Najjednoduchsi pristup s autom je asi z parkoviska blizkeho pristavu na severu, alebo priami pristup na nebetonovanych terennych cestach.',
	'x': 47.868429785506,
	'y': 17.545910735521,
}, {
	'title': 'Račerovice',
	'description': 'Po sekaná louka na freestyle i branky',
	'x': 49.2424901,
	'y': 15.8367705,
}, {
	'title': 'Radlická',
	'description': 'Menší louka dobře dostupná z metra.',
	'images': [
		'https://lh3.googleusercontent.com/D7tIkAGYl_FjXSan8pJfSVhB_-9l1uY5ZBC_p-yA-hHe3MXbbmY20hQDyGA5Om1-QIT5mjbexdlo8ORfaGQOXvv1DRgi1IJiT3pn--k_OMyRrlnMqnq6ew_apJ5iJu_c',
	],
	'x': 50.0552032,
	'y': 14.3871009,
}, {
	'title': 'Random bando',
	'description': 'POZOR v budove napravo pod vezou byvaju bezdomovci so psom. nelietat dovnutra !! inac no problem.',
	'youtube': [
		'7POnAbRrQYA',
	],
	'x': 48.1977153,
	'y': 17.1920854,
}, {
	'title': 'RC Letiště Rovensko',
	'description': 'Udržována plocha pro RC letadla.',
	'x': 49.90115492269,
	'y': 16.870273947716,
}, {
	'title': 'RC Letiště Vlčí Habřina',
	'description': 'Malé vesnické letiště pro letadla vhodné i pro drony.',
	'x': 50.081908713379,
	'y': 15.58622200524,
}, {
	'title': 'Řeporyje',
	'description': 'Slepá silnice předělená panely vedle louky. Podél silnice středně vzrostlé stromy.',
	'x': 50.0365732,
	'y': 14.2977566,
}, {
	'title': 'Rozcestí nad Přemyslovem',
	'description': 'I v zimě pěkné místo na polet. Sem tam strom nebo skupina stromů, šterk místo bláta, přístřešek na rozbalení techniky.',
	'x': 50.109946517201,
	'y': 17.056111093541,
}, {
	'title': 'Rozhľadňa',
	'youtube': [
		'TwsHBd9-1KY',
	],
	'x': 48.257811,
	'y': 18.2638425,
}, {
	'title': 'Rozsáhlý lesopark',
	'description': 'Na freestlyle nenajdete lepší místo a vyplatí se sem zajet. Po ránech a večerech liduprázdné, nicméně je třeba dbát opatrnosti, lidi sem občas chodí. O víkendech moc nedoporučuji, to je park více navštěvovaný. Ráno tu potkáte za hodinu sotva dva pejskaře nebo běžce. Park je hezky udržovaný, tráva posekaná a stromy jsou vysázeny do hezkých vzorů, je to nádherné místo na opatrné lítání.',
	'youtube': [
		'_8HqKu1oMDk',
	],
	'x': 50.2584828,
	'y': 14.3161082,
}, {
	'title': 'Roztoky',
	'description': 'Místní již dlouho používají na letadla hlavně větroně. Lehce větrno, ale miniquad nemá problém. Pokud je posekáno dá se postavit i trať.',
	'x': 50.1551294,
	'y': 14.3723649,
}, {
	'title': 'Rujny',
	'x': 50.030217883464,
	'y': 14.331369996071,
}, {
	'title': 'Rusavky',
	'description': 'Skalnatý terén, spusta stromků po okolí... vše je přístupné, žádné velké skály, dá se po nich bezpečně lézt. Z nejvyššího bodu je výhled po okolí. Lidi sem moc nechodí. Na freestyle blbnutí ideální.',
	'x': 50.2155247,
	'y': 14.295195,
}, {
	'title': 'Rusek',
	'description': 'Pole na trenink. Blizko letiste, tak litat zodpovedne a ne moc vysoko.',
	'x': 50.2436989,
	'y': 15.8617473,
}, {
	'title': 'Sad',
	'description': 'Dobre misto na zavodni trat pokud clovek nema branky. Leta se mezi stromy. Da se dojet auttem.',
	'x': 50.153490309669,
	'y': 13.911377318042,
}, {
	'title': 'Sad Satina',
	'description': 'Jabloňový sad, alej',
	'x': 49.572183077355,
	'y': 18.376493593594,
}, {
	'title': 'Sádek',
	'description': 'Vzadu bývalé škvárové hřiště, v místě několik nízkých stromů, dětské hřiště, lavičky, členitý terén. Vedle spotu domov důchodců. z druhé strany zahrada domu ( majitel nemá problém)',
	'x': 49.276549090282,
	'y': 17.391840587686,
}, {
	'title': 'Sady',
	'description': 'Zbylo tu už jen několik stromů, ale tráva tady není moc posekaná. Lítat se dá i nad přilehlých polích.',
	'x': 50.1976782,
	'y': 15.0300157,
}, {
	'title': 'Samota Chroboly',
	'description': 'Libovej spot na freestyle. Louka 100x100m a opuštěný chajdy. Pár stromů na diving. Pokud půjdete, ozvěte se na ig @mttfrssRád provedu.',
	'x': 48.945005704355,
	'y': 14.049992899536,
}, {
	'title': 'Scraggle city',
	'description': 'Jakási jáma s malými stromečky a křovím. Ohraničená z jedné strany plotem a z druhé cestou a potokem',
	'youtube': [
		'f2YLtAaaec4',
	],
	'x': 49.170611068968,
	'y': 16.670049726963,
}, {
	'title': 'Šenov',
	'description': 'Další z míst QUADLAN-OV. Přijďte si zalítat',
	'x': 49.7800725,
	'y': 18.3792686,
}, {
	'title': 'Sherwood',
	'description': 'Riedky les. Problém je s parkovaním. Smerom od NR treba spomaliť, vpravo sa nachádza bývalý vjazd do lesa kde je možné nechať auto.',
	'youtube': [
		'YmUewwBjvEc',
	],
	'x': 48.2613914,
	'y': 18.0344439,
}, {
	'title': 'Skala',
	'youtube': [
		'RzCQZXCdbKA',
	],
	'x': 49.090998,
	'y': 18.9991486,
}, {
	'title': 'Sklaka Rabakovská Louka',
	'description': 'Pekna nerovna louka s par stromy. 3 min chuze od zaparkovanyho auta nahore nad loukou. obcas nakej pejskar.',
	'youtube': [
		'a_yfz-4RI94',
	],
	'x': 50.0639782,
	'y': 14.5203102,
}, {
	'title': 'SKP Olympia',
	'description': 'Otevřený stadion,létáme tam, jen když nikoho neohrožujeme, zatím nás nikdo nevyrazil....',
	'x': 49.949086723916,
	'y': 15.250434279442,
}, {
	'title': 'Slalomový ráj Zbraslavského Zámku',
	'description': 'Zámecký sad s haldou stromů vysázených v přímých řadách, slalomový ráj. Nevýhoda je nedaleká strakonická = kravál a držet se v sadu (je dostatečně velký). Netuším, zda neni soukromé, ale oploceno není a nikdo mě vyhnat nepřišel. Civilisti v sadu v podstatě žádní, na venčení, ani romantiku to neni nic moc místo...',
	'x': 49.9800604,
	'y': 14.3918002,
}, {
	'title': 'Smeťák',
	'description': 'Dost často zde nikdo není, občas kiting a v tu chvíli je zde létání zakázáno viz web dole. Křídla jsou vidět z dálky takže je hned vidět jestli je volno. Auto zůstane pod kopcem ulice Vyderská, nahoře je pěkná lavička a stůl. Žádné překážky. Web klubu: <a href="http://www.smetak.banda.cz/" target="_blank">www.smetak.banda.cz</a>',
	'images': [
		'https://lh3.googleusercontent.com/eAuE8i2nKi2OECCbN-4P_JlYzp-K2jCOEUFLFPFvnw_IVznsEhzohmcovqGY2aAoxoB8sEuWa3Z8gzyXu1UsgFJ381A1swejtpSMrsH2HX41hIJs6YM2zLTMaHFy6LYB',
	],
	'x': 50.0284063,
	'y': 14.5839094,
}, {
	'title': 'Sobešice',
	'description': 'Velká louka, pár stromu. Je nutno dávat pozor na pejskaře.',
	'x': 49.2486746,
	'y': 16.6247392,
}, {
	'title': 'Soudňáky',
	'description': 'Louka mezi rybniky Soudný a Oborský. Stromy, voda. Pěkné místo. Dá se tam dojet i autem. Na louce lze postavit závodní trať.',
	'x': 50.165547607426,
	'y': 13.922435426034,
}, {
	'title': 'Srbec Bozi muka',
	'description': 'Louka u Bozich muk nad Srbci je idealni pro vzlet na protejsi kopec. Pozor na draty vysokeho napeti. Je to spis na midrange surfing nad lesem.',
	'x': 50.219260676012,
	'y': 13.88400213662,
}, {
	'title': 'Stará Cementáreň',
	'description': 'Ideálne pre TinyWhoop a pre skúsenejších aj s klasikou...',
	'x': 48.7267077,
	'y': 19.1541648,
}, {
	'title': 'Stara kotolna',
	'description': 'Stara budova s kominom, pristup len na peso.',
	'x': 48.1046606,
	'y': 17.2614429,
}, {
	'title': 'Stará vodáreň',
	'description': 'Bývalá vodáreň pre miestne družstvo.. už len ruina. !!! Pozor na asi 3 otvorené kanály. je v nich voda a murphyho zákony platia na 100%',
	'x': 48.1566406,
	'y': 18.6554369,
}, {
	'title': 'Staré ihrisko Šulekovo',
	'youtube': [
		'2zBU-Gdsxdo',
	],
	'x': 48.4181078,
	'y': 17.7708149,
}, {
	'title': 'Staré město - cesta v poli',
	'description': '>2km dlouhá opuštěná asfaltová cesta, ideální na testování dosahu. V okolí spousta polí na lítání, výskyt lidí nízký. ~6km daleko je letiště, proto raději nelítat moc vysoko :)',
	'x': 49.0861218,
	'y': 17.4754286,
}, {
	'title': 'Starý atletický stadion',
	'description': 'Krásná velká plocha v centru města, rušení v pohodě, parkování hned vedle 5kc na půl hodiny, 10kc na celý den',
	'x': 50.463737045987,
	'y': 13.418136693535,
}, {
	'title': 'Stražovják',
	'description': 'Odpočívadlo u hospody vybudované pod vysílačem. K tomu samozřejmě nelítat, koptéra by se blbě lovila. Stranou z kopce se ale otevírá výhled na moc hezkou krajinu. Pozor na ATZ oblast kyjovského letiště (LKKY)',
	'x': 49.011435286545,
	'y': 17.059336414086,
}, {
	'title': 'Stredný Čepeň',
	'description': 'Trávnatá lúka s pár krýkmi vhodnými ako prekážky.',
	'x': 48.3076898,
	'y': 17.7401733,
}, {
	'title': 'Suflo Hostivice',
	'description': 'Prima pole se stromy vhodné i na instalaci branek.',
	'x': 50.076091256596,
	'y': 14.2245054245,
}, {
	'title': 'Sulovske skaly',
	'youtube': [
		'eVLSzdLF7HQ',
	],
	'x': 49.1795994,
	'y': 18.6039305,
}, {
	'title': 'Šumbark',
	'description': 'Bývalá, uzavřená cesta, dá se na ní přímo zaparkovat, je relativně široká. Poblíž je i louka. Není tam moc překážek ale dá se tam dělat nějaký freestyle.',
	'x': 49.823364333756,
	'y': 18.386537369836,
}, {
	'title': 'Super spot',
	'description': 'Placek s mírnou trávou a nahodile rozhozené stromy',
	'x': 49.816535593484,
	'y': 17.502963870395,
}, {
	'title': 'Tajná hala uprostřed města',
	'description': 'Schovaná hala v rozbitě vypadajicím dvoře.',
	'x': 49.209108261043,
	'y': 16.633924474998,
}, {
	'title': 'Technologický park',
	'description': 'upravené místo s jezírkem a malým lesíkem',
	'x': 49.23026042178,
	'y': 16.576120057136,
}, {
	'title': 'Telekomunikační věž',
	'description': 'Parádní na divy, nikdo nehlídá, nicméně dole je malý domek obehnaný plotem s ostnatým drátem, takže je potřeba dávat pozor a nespadnout tam...',
	'youtube': [
		'a8AS0FPAeuI',
	],
	'x': 50.2267655,
	'y': 14.315663,
}, {
	'title': 'Tex Bando',
	'description': 'Zatiaľ najlepšie bando aké som doteraz našiel. A to už rok hľadám. Miesto je voľne prístupné. Nie je tam žiadna ochranka. Miesto je prístupné 24/7. V okolí budovy sídlia iné firmy, tak okolo chodia ľudia a autá. Pokiaľ nestojíte na ceste, tak je to v pohode. Dá sa dostať na každé poschodie (asi okrem strechy, neskúšal som). Pozor, v pilieroch sú železné / kovové drôty, ktoré vytŕčajú!',
	'youtube': [
		'oGiTH9BEztI',
	],
	'x': 49.088288492677,
	'y': 19.272933902198,
}, {
	'title': 'Trafačka v Otvicích',
	'description': 'Stará trafostanice, právě se zde staví nová silnice, stále se zde dá krásně lítat',
	'x': 50.479410131542,
	'y': 13.441639621377,
}, {
	'title': 'Training ground',
	'description': 'Byvale modelarske letisko, nizka trava, sem tam pokosia.',
	'youtube': [
		'KdML6QslzNg',
	],
	'x': 48.4027784,
	'y': 17.3688698,
}, {
	'title': 'Tree Tunnel',
	'youtube': [
		'f7rTMD7mWgE',
	],
	'x': 48.2877406,
	'y': 18.0240315,
}, {
	'title': 'Trees paradise',
	'description': 'Perfektné miesto na freestyle alebo racing. Je tu veľa stromov, ktoré sú dostatočne od seba a nikto tu nechodí. Väčšinou je tráva pokosená, takže ideál.',
	'youtube': [
		'erub_2w-WcQ',
	],
	'x': 49.06693570839,
	'y': 19.289050306414,
}, {
	'title': 'Trees place',
	'description': 'Ďalšie dobré miesto na lietanie medzi stromami, vedľa nich a popod ne. Opustené miesto. Nikto tu nechodí.',
	'x': 49.085214486872,
	'y': 19.253001060397,
}, {
	'title': 'Třešňovka Hostivice',
	'description': 'Super místo na tréning bez branek, spousta stromů travnatá udržovaná plocha. Lze nabíjet z auta.',
	'images': [
		'https://lh3.googleusercontent.com/poOlSJE1JflbRwMEqQRGnPNcGMCZlBZBbA2_l8q08mT1xKVUs3hvdx6inDmmqGcMAfGLA0wg6xzdQxNkTEQc_Z3IKAGeUV8xtCCEDvDEkGM57KkCHxfsC40p6QsOksa7Cw',
	],
	'x': 50.0851516,
	'y': 14.2598328,
}, {
	'title': 'Trubky HK',
	'description': 'Políčko kde se da vylbnout s teplovodem - trubkama:-) K tomu políčko.',
	'youtube': [
		'LvjzAEOci_w',
	],
	'x': 50.222677291138,
	'y': 15.850218542305,
}, {
	'title': 'Tunel & Dráty',
	'description': 'Štěrkovka obklopená stromama, tvořící krásnej tunel. To vše po okrajich louky obklopeno sloupama vyskoyho vedeni, ktery primo vybizej k divnuti. Jako bonus celkem malej dolik kde je absolutni klid a da se litat i solo.',
	'x': 49.747577960626,
	'y': 13.312810819224,
}, {
	'title': 'U Křivé Borovice',
	'description': 'Provoz nízký až střední, hlavně pejskaři, procházkáři a jezdci na koních z blízké stáje. Spousta možností parkování, které občas okupují milenci s vidinou soukromí, nebo partičky omladiny. Létal jsem zatím jen na začátku pole, vlevo po vjezdu na odbočku, bez protestů (prostě tam, kde je značka). Modeláři se tu vyskytují docela často.',
	'x': 49.2209047,
	'y': 16.492517,
}, {
	'title': 'U paloučku',
	'description': 'Menší travnatá plocha ideální pro začátky. Snadný přístup, blízko centra.',
	'x': 50.1506507,
	'y': 13.8953662,
}, {
	'title': 'U Prokopáku',
	'description': 'Pěkné místo na zalétání i menší trať. Jedna lavička, 3 stromy.',
	'x': 50.0441322,
	'y': 14.3541071,
}, {
	'title': 'U Rataj',
	'description': 'Otevřené pole, vhodné i pro větší drony. Auto lze nechat pod stromy u křížku na vjezdu na polní cestu. Žádné překážky v okolí, z jižní strany prostor limitován frekventovanou silnicí, doporučuji jít polní cestou dál kousek na západ, nebo lítat striktně jen na severní stranu od křížku.',
	'x': 49.589595197791,
	'y': 17.104376409442,
}, {
	'title': 'U Slovanu',
	'description': 'Divoka trava, kterou za celou sezonu nikdo neposece, ale paradni spot na jaro a podzim.',
	'x': 50.7765678,
	'y': 15.0476646,
}, {
	'title': 'U vodárny',
	'description': 'Tři pole, létá se dle toho, co tam kde zrovna roste.',
	'x': 49.952538431918,
	'y': 15.246271491051,
}, {
	'title': 'Váh Madunice',
	'youtube': [
		'Xq1VRsIqoJo',
	],
	'x': 48.4691299,
	'y': 17.7975726,
}, {
	'title': 'Váh Siladice',
	'youtube': [
		'E58m0J54Mr0',
	],
	'x': 48.3574378,
	'y': 17.7514842,
}, {
	'title': 'Vajnorske letisko',
	'description': 'Stare letisko, v zadnej casti je zakladna NATO smerom na Bratislavu zas oficialne RC letisko. Oboma smermi som zazil rusenie signalu. Lietat radsej len nad travnatou plochou, pripadne freestyle opatrne v okoli starych budov a hangarov. Budovy su v blizkosti cesty, tak opatrne',
	'x': 48.2021954,
	'y': 17.1875525,
}, {
	'title': 'Vavřinec u Domažlice',
	'description': 'Velká louka s pár stromama, občas nějaký pejskaři, ale dost málo.',
	'x': 49.4188884,
	'y': 12.9090321,
}, {
	'title': 'Vedle Crossové tratě',
	'description': 'Topové místečko na zalítání freestylu.',
	'youtube': [
		'cEuMcnpjOtU',
	],
	'x': 49.886298448671,
	'y': 16.842326670885,
}, {
	'title': 'Velká louka se spoustou stromů',
	'description': 'Perfektní místo na lítání, žádní lidi, spousta menších stromů, polní cesta... postavit se dá nahoře na kopci odkud je dobrý výhled směrem dolu a je tu hodně velký prostor na rychlé freestyle létání kolem přirozených překážek (stromů)...',
	'x': 50.2280696,
	'y': 14.2833745,
}, {
	'title': 'Velký volný prostor pro začátečníky',
	'description': 'Obrovské pole bez elektrických drátů, vhodné pro začátečníky, dron nemůže nikam uletět.',
	'x': 49.752277543755,
	'y': 13.29046416174,
}, {
	'title': 'Větrné elektrárny',
	'description': 'Louky s elektrárnami',
	'x': 50.446396987096,
	'y': 13.160092413069,
}, {
	'title': 'Větrné elektrárny Pchery',
	'description': 'Chcete si zkusit dive obrovské větrné elektrárny? Tak hurá na to. Jsou zde dvě, pozemek není oplocený a je volně přístupný. Lítal jsem zde v době, kdy zde pán servisoval elektrárny a chodil dovnitř. Čekal jsem že dostanu vyhubováno a bude odchod, ale nic, pán mě nechal lítat a měl jsem stanoviště asi 100m od něj...',
	'youtube': [
		'm0r0aDmmJJ4',
	],
	'x': 50.1963321,
	'y': 14.1290832,
}, {
	'title': 'Větrný mlýn Příčovy',
	'description': 'Zřícenina větrného mlýna + přilehké louky. Občas někdo projde nebo přijede, udělá pár fotek a odjede, takže celkem klidné místo.',
	'youtube': [
		'4SZGgISI6Qg',
	],
	'x': 49.67548076621,
	'y': 14.38168823719,
}, {
	'title': 'Vidoule',
	'description': 'Pole uprostřed Prahy, kde je málo míst k létání. Uprostřed pole je hezká cesta lemovaná stromy okolo kterých se dá blbnout a prolétat mezi nimi. Pro blbnutí s cestou je nutný dobrý spotter, bez něj je to riskantní, protože po cestě chodí hodně chodců, pejskařů, nebo cyklistů. Zábavná je i hradba stromů na okraji pole, která se dá například divovat.',
	'x': 50.059187183889,
	'y': 14.353730504955,
}, {
	'title': 'Vodáreň BITA',
	'description': 'Zaujímavé miesto na polet. Opustená vodáreň bez okien. Chodia okolo iba záhradkári z miestnej osady cez víkendy viac ako obvykle. Samotné budovy ležia na súkromnom pozemku ale sú prístupné a zatial problém nebol.',
	'youtube': [
		'Ye3P8TG356w',
	],
	'x': 48.2615021,
	'y': 18.0814737,
}, {
	'title': 'Vrch Vysoká',
	'description': 'Pěkné místo na polétání, občas se zde vyskytují i jiní modeláři.',
	'x': 49.943004212436,
	'y': 15.187059044838,
}, {
	'title': 'Vyhlídka na Brno',
	'description': 'Parcela dobrá na vlajky',
	'x': 49.2155753,
	'y': 16.6712004,
}, {
	'title': 'Vyhlídka nad Hrbovem',
	'description': 'Super místo u kapličky. Je tam super kamenej stůl na krámy a je ve stínu.',
	'x': 49.361624554098,
	'y': 15.957182595449,
}, {
	'title': 'Vyhořelá zřícenina',
	'description': 'Vyhořelý jednopodlažní dům bez střechy, parkovat lze v ulici Na Balkáně hned u spotu.',
	'images': [
		'https://live.rotorama.cz/wp-content/uploads/2021/04/fpv-1024x768.jpeg',
	],
	'x': 50.095472,
	'y': 14.493472,
}, {
	'title': 'Vyjhlídka a sjezdovka',
	'description': 'Sjezdovka na kopci u Božího daru, stromy, louky, nádherná příroda',
	'x': 50.405068396391,
	'y': 12.932366366885,
}, {
	'title': 'Vypich',
	'description': 'Místo perfektně dostupné jak autem, tak hromadnou dopravou. Pokud jedete autem, tak vězte, že do ulice Na Vypichu je zakázaný vjezd. Trošku to svádí ten zákaz ignorovat, ale bohužel tam občas kempují policisté a čekají na lidi kteří si tou ulicí urychlují cestu přes křižovatku. Ti samozřejmě nemají úplně pochopení pro to, že tam chce člověk zaparkovat. Lepší je parkovat někde kolem Zvoníčkovy, a dojít tam z druhé strany. Co se týká lidí, tak je tam klid, sem tam nějaký pejskař projde po cestě, tempem jeden za 15 minut. Místo vhodné i pro to vzít tam děti, nemají moc kam utéct, zatímco máte brýle na hlavě -)Výhodou je i dostatek malých stromků na slalom.',
	'x': 50.0808282,
	'y': 14.3481016,
}, {
	'title': 'Vyškov - tovární zona - Van Leeuwen',
	'description': 'Plac za firmou Van Leeuwen ve Vyškově, spíše menší, spousta stromku, keřu, staré koleje. Udržované, parkování hned u spotu.',
	'x': 49.287355685741,
	'y': 17.000474173127,
}, {
	'title': 'Vyškov - u ZOO',
	'description': 'Velký a členitý spot u Vyškovské ZOO, parkování u firmy RAMI. Pozor na pejskaře a el. vedení.',
	'x': 49.271462055921,
	'y': 16.995899744874,
}, {
	'title': 'Vyškov - za školou Letní pole',
	'description': 'Vedle školy je plac, kde měly kdysi vyrůst bytové domy. Na to ale nedošlo, teď je tu louka, stromy, keře a torzo kanalizačních skruží. Místo je udřované, takže hledání spadlého dronu není problém. Jednom poroz na pejskaře, děti a přilehlé bytové domy',
	'x': 49.280003485821,
	'y': 16.979932044551,
}, {
	'title': 'Za Ekonomickou',
	'description': 'Pekné rozľahlé pole. V lete ho kosia a chodia tam bežne modelári lietadiel. Skvelé na začiatky alebo na postavenie trate. Prístup autom',
	'x': 48.1260758,
	'y': 17.1391654,
}, {
	'title': 'Za hrochem',
	'description': 'Mensi placek pro 1-2 lidi, obcas byva posekanej, spis pro letani u zemi. Obcasny vyskyt pejska nebo pruchodce. Spis pro mensi stroje',
	'x': 49.2124609,
	'y': 16.5588507,
}, {
	'title': 'Za psychynou',
	'description': 'stromy krovi atp..',
	'x': 49.2969684,
	'y': 17.3738801,
}, {
	'title': 'Zábřeh - Louka & Stromy',
	'description': 'Docela veliká louka s dobře rozmístěnými stromy. Kolem můžou procházet lidi...',
	'x': 49.8693007,
	'y': 16.856073,
}, {
	'title': 'Zábřeh - Motocrossová trať',
	'images': [
		'https://lh4.googleusercontent.com/CVSH_6yf5YX3UlkXlAVVZ3Tlg_vup3kueZR1YdzPA2Pd6w3kQEylZzZjfUghNdAaDUhGCBr4AzQfVKp0y0tzOIKqvqkkZXSZTrRU9rkoOIEaglMn3B0m9L6QDieIaeg',
	],
	'x': 49.8905081,
	'y': 16.8405497,
}, {
	'title': 'Záhorie',
	'description': 'Super miesto prístup autom bez problémov sú tu riedke stromy, proste paráda.',
	'x': 48.5489513,
	'y': 17.1403885,
}, {
	'title': 'Zeleneč',
	'description': 'Kopeček, nízké stromy. Určené spíše pro rekreační zalítání. Nic velkého. Ale zatím vždy bez problémů.',
	'x': 50.1444149,
	'y': 14.6870416,
}, {
	'title': 'Železničný most',
	'description': 'A s miestom na zaparkovanie :) pozor na vodu.',
	'youtube': [
		'OKVX6aKvWT8',
	],
	'x': 48.5279384,
	'y': 19.0052211,
}, {
	'title': 'Železničný most Hlohovec',
	'youtube': [
		'y-JxhOzjhdw',
	],
	'x': 48.4391378,
	'y': 17.7926159,
}, {
	'title': 'Zřícenina kaple Povýšení sv. Kříže',
	'description': 'Malo ludi, obcas vyletnici z turistickej cesty, ale s trosku opatrnosti nebude problem. Je tu mozne lietat nad volne pole, alebo v hustom lese a klickovat medzy steny zruceniny, takze moznosti pre zaciatocnikov, i pre precizne lietanie. Jedine chyba nieco medzi tie extremy. S autom sa da zabocit z cesty 272 na polnu cestu, a priblizovat podla vlastnej uvahy, a aktualneho stavu cesty.',
	'x': 50.137965181261,
	'y': 14.8512840271,
}, {
	'title': 'Zúgov Nové Zámky',
	'description': 'Prístup je možný aj autom ja tu lietam pravidelne.',
	'youtube': [
		'fWNvomrOUdQ',
	],
	'x': 49.651663873211,
	'y': 18.260607743652,
}, {
	'title': 'Žuráň',
	'description': 'Výborné místo třeba pro jednoho nebo dva lidi. Malé, ale stromy, sloupy.<br>Problém je provoz, který je velký. Nejezdím tady nejčastěji, ale zatím se mi zadařilo jen jednou večer že tu nikdo nebyl.',
	'youtube': [
		'QQ0BsvjpvtA',
	],
	'x': 49.1797678,
	'y': 16.7380169,
}];

const bounds = [
	[47.7319344794918, 12.0904541015625],
	[51.0559626423052, 22.5656604766846],
];

const map = L.map('map', {
	'layers': [
		L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {'attribution': '&copy; <a href="https://www.openstreetmap.org/copyright" target="_blank">OpenStreetMap</a> contributors'}),
	],
	'minZoom': 7,
});

map.fitBounds(bounds);

for (const marker of markers) {
	let icon = blue;

	let html = `<h3>${marker['title']}</h3>`;

	if (marker['description']) {
		html += `<p>${marker['description']}</p>`;
	}

	if (marker['images']) {
		icon = gold;

		for (const image of marker['images']) {
			if (typeof image == 'string') {
				html += `<img src="${image}">`;
			} else if (image['link'] && image['image']) {
				html += `<a href="${image['link']}" target="_blank"><img src="${image['image']}"></a>`;
			} else if (image['link']) {
				html += `<a href="${image['link']}" target="_blank">Fotky</a>`;
			} else if (image['image']) {
				html += `<img src="${image['image']}">`;
			}
		}
	}

	if (marker['youtube']) {
		icon = green;

		for (const video of marker['youtube']) {
			html += `<iframe width="700" height="400" src="https://www.youtube.com/embed/${video}"></iframe>`;
		}
	}

	if (marker['arena']) {
		icon = red;
	}

	L.marker([marker['x'], marker['y']], {'icon': icon})
		.addTo(map)
		.bindPopup(L.popup({
			'content': html,
			'maxWidth': 750,
		}));
}

document.addEventListener('keydown', (e) => {
	if (e.key === 'Escape') {
		document.querySelector('#startup-dialog-close').checked = true;
	}
}, false);
